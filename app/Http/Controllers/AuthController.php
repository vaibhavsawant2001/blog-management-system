<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use User;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'password' => 'required',
        ]);
        if (Auth::attempt(['name' => $request->name, 'password' => $request->password])) {
            return redirect()->intended('yaaaro_pms/admin');
        } else {
            return redirect('/')->with('error', 'Invalid credentials');
        }
    }
    public function logout()
    {
        Auth::logout();

        return redirect('/');
    }
}
