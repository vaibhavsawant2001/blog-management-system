<?php

namespace App\Http\Controllers;

use App\Models\BlogCategory;
use Illuminate\Http\Request;

class BlogCategoryController extends Controller
{
    public function index(Request $request)
    {
        $blogCatogery = BlogCategory::all();
        $data = compact('blogCatogery');
        return view('yaaaro_pms/blog_category')->with($data);
    }
    public function store(Request $request)
    {
        $request->validate([
            'blog_category' => 'required',
        ]);

        $data = [
            'blog_category' => $request->input('blog_category'),
        ];
        $blogCatogery = BlogCategory::create($data);
        return redirect('yaaaro_pms/blog_category')->with('success', 'Added Successfully');
    }
    public function update(Request $request, $id)
    {
        $request->validate([
            'blog_category' => 'required',
        ]);

        $blogCategory = BlogCategory::find($id);

        if (!$blogCategory) {
            return redirect('yaaaro_pms/blog_category')->with('error', 'Category not found');
        }

        $blogCategory->blog_category = $request->input('blog_category');
        $blogCategory->save();

        return redirect('yaaaro_pms/blog_category')->with('success', 'Updated Successfully');
    }
    public function edit($id)
    {
        $blogCategory = BlogCategory::findOrFail($id);
        $data = compact('blogCategory');
        return view('yaaaro_pms/edit_subject_category')->with($data);
    }
    public function destroy($id){
        $blogCategory = BlogCategory::find($id);
        $blogCategory->delete();
        return redirect('yaaaro_pms/blog_category')->with('success','Deleted Successfully');
    }
    public function updateStatus(Request $request, $id)
    {
        $model = BlogCategory::find($id);
    
        if ($model) {
            $model->status = $model->status == 0 ? 1 : 0;
            $model->save();
    
            $message = 'Status updated successfully.';
            return redirect()->back()->with('success', $message);
        }
    
        return redirect()->back()->with('error', 'Record not found.');
    }
}
