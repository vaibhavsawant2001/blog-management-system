<?php

namespace App\Http\Controllers;

use App\Http\Requests\BlogRequest;
use App\Models\Blog;
use App\Models\Writers;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function index(Request $request)
    {
        $data = Blog::with('writer')->get();
        $alldata = compact('data');
        return response()->json($alldata, 200);
    }
    public function show(Request $request, $id)
    {
        $blog = Blog::with('writer')->find($id);

        if (!$blog) {
            return response()->json(['error' => 'Blog not found'], 404);
        }

        $data = compact('blog');
        return response()->json($data, 200);
    }
    public function store(BlogRequest $request)
    {
        $blogData = $request->only([
            'subject',
            'topic',
            'keyword',
            'assign_to',
            'deadline',
            'blog_category',
            'comment',
        ]);
        $writer = Writers::where('writers_name', $request->input('assign_to'))->first();
        if ($writer) {
            $blogData['writers_id'] = $writer->writers_id;
            $blog = Blog::create($blogData);
            $writer->increment('blog_count');
            $writer->save();
            return redirect('yaaaro_pms/blogs')->with('success', 'Added Successfully');
        }
        return redirect('yaaaro_pms/blogs')->with('error', 'Writer not found');
    }
    public function destroy(Request $request, $id)
    {
        $blog = Blog::find($id);
        $blog->delete();
        return redirect('yaaaro_pms/blogs')->with('success', 'Deleted Successfully');
    }
    public function edit($id)
    {
        $blog = Blog::findOrFail($id);
        $writers = Writers::all();
        $data = compact('blog', 'writers');
        return view('yaaaro_pms/edit_blogs')->with($data);
    }
    public function update(BlogRequest $request, $id)
    {
        $blog = Blog::find($id);
    
        if (!$blog) {
            return redirect('yaaaro_pms/blogs')->with('error', 'Blog not found');
        }
    
        $oldWriter = Writers::where('writers_name', $blog->assign_to)->first();
        $newWriter = Writers::where('writers_name', $request->input('assign_to'))->first();
    
        $blogData = [
            'subject' => $request->input('subject'),
            'topic' => $request->input('topic'),
            'keyword' => $request->input('keyword'),
            'assign_to' => $request->input('assign_to'),
            'deadline' => $request->input('deadline'),
            'blog_category' => $request->input('blog_category'),
            'comment' => $request->input('comment'),
        ];
    
        $blog->update($blogData);
    
        if ($oldWriter) {
            $oldWriter->decrement('blog_count');
            $oldWriter->save();
        }
    
        if ($newWriter) {
            $newWriter->increment('blog_count');
            $newWriter->save();
            $blog->update(['writers_id' => $newWriter->writers_id]);
        } else {
            return redirect('yaaaro_pms/blogs')->with('error', 'New writer not found');
        }
    
        return redirect('yaaaro_pms/blogs')->with('success', 'Updated Successfully');
    }
    
    public function showPendingBlogs($writer_id)
    {
        $writer = Writers::find($writer_id);
        $pendingBlogs = $writer->blogs->where('status', 0);
        return view('yaaaro_pms/pending_blogs', compact('pendingBlogs'));
    }
    public function showCompletedBlogs($writer_id)
    {
        $writer = Writers::find($writer_id);
        $completedBlogs = $writer->blogs->where('status', 1);
        return view('yaaaro_pms/complete_blogs', compact('completedBlogs'));
    }
}
