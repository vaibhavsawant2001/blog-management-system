<?php

namespace App\Http\Controllers;

use App\Models\Writers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);
        $email = $request->input('email');
        $password = $request->input('password');
        $writer = Writers::where('email', $email)->where('password', $password)->first();
    
        if ($writer) {
            return redirect('writers_pms/admin');
        } else {
            return back()->withErrors(['email' => 'Invalid credentials']);
        }
    }
    public function logout()
    {
        Auth::logout();

        return redirect('/');
    }
}
