<?php

namespace App\Http\Controllers;

use App\Models\Status;
use Illuminate\Http\Request;

class StatusController extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
        ]);

        $data = [
            'title' => $request->input('title'),
            'status' => $request->has('status') ? 1 : 0,
        ];

        $status = Status::create($data);

        return redirect('yaaaro_pms/status')->with('success', 'Added Successfully');
    }
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
        ]);

        $status = Status::findOrFail($id);

        $data = [
            'title' => $request->input('title'),
            'status' => $request->has('status') ? 1 : 0,
        ];

        $status->update($data);

        return redirect('yaaaro_pms/status')->with('success', 'Updated Successfully');
    }
    public function edit($id)
    {
        $status = Status::findOrFail($id);
        $data = compact('status');
        return view('yaaaro_pms/edit_status')->with($data);
    }
}
