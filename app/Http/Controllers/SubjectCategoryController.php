<?php

namespace App\Http\Controllers;

use App\Models\SubjectCategory;
use Illuminate\Http\Request;

class SubjectCategoryController extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'subject_name' => 'required',
        ]);

        $data = [
            'subject_name' => $request->input('subject_name'),
        ];
        $subject = SubjectCategory::create($data);
        return redirect('yaaaro_pms/subject_category')->with('success', 'Added Successfully');
    }
    public function edit($id)
    {
        $SubjectCategory = SubjectCategory::findOrFail($id);
        $data = compact('SubjectCategory');
        return view('yaaaro_pms/edit_subject_category')->with($data);
    }
    public function update(Request $request, $id)
    {
        $request->validate([
            'subject_name' => 'required',
        ]);

        $subject = SubjectCategory::findOrFail($id);

        $data = [
            'subject_name' => $request->input('subject_name'),
        ];

        $subject->update($data);

        return redirect('yaaaro_pms/subject_category')->with('success', 'Updated Successfully');
    }
    public function destroy($id){
        $subject = SubjectCategory::findOrFail($id);
        $subject->delete();
        return redirect('yaaaro_pms/subject_category')->with('success','Deleted Successfully');
    }
    public function updateStatus(Request $request, $id)
    {
        $model = SubjectCategory::find($id);
    
        if ($model) {
            // Toggle the status: if it's 0, change it to 1; if it's 1, change it to 0
            $model->status = $model->status == 0 ? 1 : 0;
            $model->save();
    
            $message = 'Status updated successfully.';
            return redirect()->back()->with('success', $message);
        }
    
        return redirect()->back()->with('error', 'Record not found.');
    }

}
