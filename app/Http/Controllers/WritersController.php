<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\Writers;
use Illuminate\Http\Request;

class WritersController extends Controller
{
    public function index(Request $request)
    {
        $data = Writers::with('blogs')->get();
        $alldata = compact('data');
        return response()->json($alldata, 404);
    }

    public function show(Request $request, $id)
    {
        $writer = Writers::with('blogs')->find($id);
    
        if (!$writer) {
            $data = [
                'message' => 'Writer not found',
                'writer' => null,
            ];
            return response()->json($data, 404);
        }
    
        $data = [
            'message' => 'Writer found successfully',
            'writer' => $writer,
        ];
    
        // Check if the request wants JSON response
        if ($request->wantsJson()) {
            return response()->json($data, 200);
        }
    
        return view('yaaaro_pms.writers_details')->with($data);
    }
    
    public function store(Request $request)
    {
        $request->validate([
            'writers_name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'comment' => 'required',
            'address' => 'required',
            'password' => 'required',
            'bank_name' => 'required',
            'account_no' => 'required',
            'acc_holdr_name' => 'required',
            'ifsc' => 'required',
            'upi_no' => 'required',
            'blog_type.*' => 'required',
            'price.*' => 'required',
        ]);
    
        $blogTypeArray = $request->input('blog_type');
        $priceArray = $request->input('price');
    
        // Create an array to store the formatted blog_data
        $blogData = [];
        foreach ($blogTypeArray as $key => $blogType) {
            $blogData[] = [
                'blog_type' => $blogType,
                'price' => $priceArray[$key],
            ];
        }
    
        $data = [
            'writers_name' => $request->input('writers_name'),
            'email' => $request->input('email'),
            'phone' => $request->input('phone'),
            'address' => $request->input('address'),
            'bank_name' => $request->input('bank_name'),
            'account_no' => $request->input('account_no'),
            'acc_holdr_name' => $request->input('acc_holdr_name'),
            'ifsc' => $request->input('ifsc'),
            'upi_no' => $request->input('upi_no'),
            'comment' => $request->input('comment'),
            'password' => $request->input('password'),
            'blog_data' => $blogData,
        ];
    
        Writers::create($data);
    
        return redirect('yaaaro_pms/writers')->with('success', 'Added Successfully');
    }
    
    
    
    
    
    
    
    public function destroy(Request $request, $id)
    {
        $writer = Writers::find($id);
        $writer->delete();
        return redirect('yaaaro_pms/writers')->with('success', 'Deleted Successfully');
    }
    public function edit($id)
    {
        $writer = Writers::findOrFail($id);

        return view('yaaaro_pms/edit_writers', compact('writer'));
    }
    public function update(Request $request, $id)
    {
        $request->validate([
            'writers_name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'bank_name' => 'required',
            'account_no' => 'required',
            'acc_holdr_name' => 'required',
            'ifsc' => 'required',
            'upi_no' => 'required'
        ]);
        $writer = Writers::findOrFail($id);
        $data = [
            'writers_name' => $request->input('writers_name'),
            'email' => $request->input('email'),
            'phone' => $request->input('phone'),
            'address' => $request->input('address'),
            'bank_name' => $request->input('bank_name'),
            'account_no' => $request->input('account_no'),
            'acc_holdr_name' => $request->input('acc_holdr_name'),
            'ifsc' => $request->input('ifsc'),
            'upi_no' => $request->input('upi_no'),
        ];
        $writer->update($data);
        return redirect('yaaaro_pms/writers')->with('success', 'Updated Successfully');
    }
}
