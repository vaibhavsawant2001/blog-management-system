<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    use HasFactory;
    protected $primaryKey = 'blog_id';
    protected $fillable = [
        'subject',
        'topic',
        'keyword',
        'assign_to',
        'deadline',
        'blog_category',
        'comment',
        'writers_id',
    ];
    public function writer()
    {
        return $this->belongsTo(Writers::class, 'writers_id');
    }
}
