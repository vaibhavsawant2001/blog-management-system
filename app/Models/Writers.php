<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Writers extends Model
{
    use HasFactory;
    protected $primaryKey = 'writers_id';
    protected $casts = [
        'blog_data' => 'json',
    ];
    protected $fillable = [
        'writers_name',
        'email',
        'phone',
        'comment',
        'address',
        'password',
        'blog_type',
        'price',
        'bank_name',
        'account_no',
        'acc_holdr_name',
        'ifsc',
        'upi_no',
        'blog_data',
    ];
    public function blogs()
    {
        return $this->hasMany(Blog::class, 'writers_id');
    }
}
