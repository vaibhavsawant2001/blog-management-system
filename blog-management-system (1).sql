-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 11, 2023 at 02:39 PM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.1.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blog-management-system`
--

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `blog_id` bigint(20) UNSIGNED NOT NULL,
  `writers_id` bigint(20) UNSIGNED NOT NULL,
  `subject` text NOT NULL,
  `topic` text NOT NULL,
  `keyword` text NOT NULL,
  `assign_to` varchar(255) NOT NULL,
  `deadline` varchar(255) NOT NULL,
  `blog_category` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`blog_id`, `writers_id`, `subject`, `topic`, `keyword`, `assign_to`, `deadline`, `blog_category`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Blogs are a type of regularly updated websites that provide insight into a certain topic. The word blog is a combined version of the words “web” and “log.” At their inception, blogs were simply an online diary where people could keep a log about their daily lives on the web.', 'Blogs are a type of regularly updated websites that provide insight into a certain topic. The word blog is a combined version of the words “web” and “log.” At their inception, blogs were simply an online diary where people could keep a log about their daily lives on the web.', 'testtesttesttesttesttesttesttesttesttesttesttesttesttesttest', 'vaibhav sawant', '2023-12-09', '120 to 150', 1, '2023-12-09 14:32:40', '2023-12-09 16:01:15'),
(2, 1, 'Blogs are a type of regularly updated websites that provide insight into a certain topic. The word blog is a combined version of the words “web” and “log.” At their inception, blogs were simply an online diary where people could keep a log about their daily lives on the web.', 'Blogs are a type of regularly updated websites that provide insight into a certain topic. The word blog is a combined version of the words “web” and “log.” At their inception, blogs were simply an online diary where people could keep a log about their daily lives on the web.', 'htthth', 'vaibhav sawant', '2023-12-09', '260 to 300', 1, '2023-12-09 14:48:29', '2023-12-09 15:51:54'),
(3, 3, 'Blogs are a type of regularly updated websites that provide insight into a certain topic. The word blog is a combined version of the words “web” and “log.” At their inception, blogs were simply an online diary where people could keep a log about their daily lives on the web.', 'dvsvdsvdBlogs are a type of regularly updated websites that provide insight into a certain topic. The word blog is a combined version of the words “web” and “log.” At their inception, blogs were simply an online diary where people could keep a log about their daily lives on the web.', 'vishalvishalvishal', 'sharad', '2023-12-09', '160 to 250', 0, '2023-12-09 15:45:14', '2023-12-09 15:45:14'),
(4, 3, 'Blogs are a type of regularly updated websites that provide insight into a certain topic. The word blog is a combined version of the words “web” and “log.” At their inception, blogs were simply an online diary where people could keep a log about their daily lives on the web.', 'Blogs are a type of regularly updated websites that provide insight into a certain topic. The word blog is a combined version of the words “web” and “log.” At their inception, blogs were simply an online diary where people could keep a log about their daily lives on the web.', 'htthth', 'sharad', '2023-12-09', '120 to 150', 0, '2023-12-09 15:46:07', '2023-12-09 15:46:07'),
(5, 2, 'Blogs are a type of regularly updated websites that provide insight into a certain topic. The word blog is a combined version of the words “web” and “log.” At their inception, blogs were simply an online diary where people could keep a log about their daily lives on the web.', 'Blogs are a type of regularly updated websites that provide insight into a certain topic. The word blog is a combined version of the words “web” and “log.” At their inception, blogs were simply an online diary where people could keep a log about their daily lives on the web.', 'fadfadf', 'vishal pawar', '2023-12-12', '120 to 150', 0, '2023-12-09 15:49:10', '2023-12-09 16:00:26'),
(6, 1, 'home', 'homeeee', 'thtjhtjtrj6t', 'vaibhav sawant', '2023-12-12', 'hdthtdhtdh', 0, '2023-12-09 17:35:48', '2023-12-09 17:35:48'),
(7, 1, '123', '321', 'thtjhtjtrj6t', 'vaibhav sawant', '2023-12-12', 'hdthtdhtdh', 0, '2023-12-09 17:38:52', '2023-12-09 17:38:52'),
(8, 2, 'travlling', 'trst', 'test', 'vishal pawar', '2023-12-15', '120 to 150', 0, '2023-12-09 17:43:27', '2023-12-09 17:43:27'),
(9, 1, 'tsgfvgdfvsd vcd', 'dfsdfsdvsfgvfgvrfv', 'dcvdsvsdfv', 'vaibhav sawant', '2023-12-12', 'dvcdvcdvcdv', 0, '2023-12-11 13:50:05', '2023-12-11 13:50:05'),
(10, 2, 'travlling', 'tegrgergreg', 'testtesttesttesttesttesttesttesttesttesttesttesttesttesttest', 'vishal pawar', '2023-12-22', '120 to 150', 0, '2023-12-11 13:52:12', '2023-12-11 13:52:12'),
(11, 1, 'tsgfvgdfvsd vcd', 'dfsdfsdvsfgvfgvrfv', 'dcvdsvsdfv', 'vaibhav sawant', '2023-12-12', 'dvcdvcdvcdv', 0, '2023-12-11 14:35:46', '2023-12-11 14:35:46'),
(12, 1, 'travlling', 'wefefv', 'testtesttesttesttesttesttesttesttesttesttesttesttesttesttest', 'vaibhav sawant', '2023-12-13', '800', 0, '2023-12-11 14:37:11', '2023-12-11 14:37:11'),
(13, 1, 'travlling', 'ryjyrj', 'testtesttesttesttesttesttesttesttesttesttesttesttesttesttest', 'vaibhav sawant', '2023-12-14', '1000', 0, '2023-12-11 14:38:32', '2023-12-11 14:38:32');

-- --------------------------------------------------------

--
-- Table structure for table `blog_categories`
--

CREATE TABLE `blog_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `blog_category` text NOT NULL,
  `price` text NOT NULL,
  `status` text NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blog_categories`
--

INSERT INTO `blog_categories` (`id`, `blog_category`, `price`, `status`, `created_at`, `updated_at`) VALUES
(1, '800', '250', '1', '2023-12-11 13:33:31', '2023-12-11 13:33:31'),
(2, '1000', '350', '1', '2023-12-11 13:34:16', '2023-12-11 13:34:16'),
(3, '1500', '500', '1', '2023-12-11 18:04:34', '2023-12-11 18:04:34'),
(4, '2000', '750', '1', '2023-12-11 19:57:49', '2023-12-11 19:57:49');

-- --------------------------------------------------------

--
-- Table structure for table `blog_writer`
--

CREATE TABLE `blog_writer` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_reset_tokens_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(14, '2023_12_04_095036_create_writers_table', 2),
(16, '2023_12_05_114127_blogs', 3),
(17, '2023_12_06_093944_create_blog_writer_table', 3),
(18, '2023_12_09_091740_create_subject_categories_table', 4),
(19, '2023_12_09_100034_create_writers_categories_table', 5),
(20, '2023_12_10_080151_create_blog_categories_table', 6);

-- --------------------------------------------------------

--
-- Table structure for table `password_reset_tokens`
--

CREATE TABLE `password_reset_tokens` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `token` varchar(64) NOT NULL,
  `abilities` text DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subject_categories`
--

CREATE TABLE `subject_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `subject_name` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subject_categories`
--

INSERT INTO `subject_categories` (`id`, `subject_name`, `status`, `created_at`, `updated_at`) VALUES
(3, 'test', 0, '2023-12-11 15:10:01', '2023-12-11 15:31:46');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'bluesun', 'bluesun@gmail.com', NULL, '$2y$12$7145TE55xATiVidQfUlKUuvxkkv1YeuxydgKcCIEoTcpxuTA9LH1m', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `writers`
--

CREATE TABLE `writers` (
  `writers_id` bigint(20) UNSIGNED NOT NULL,
  `writers_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(254) DEFAULT NULL,
  `phone` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `account_no` varchar(255) NOT NULL,
  `acc_holdr_name` varchar(255) NOT NULL,
  `ifsc` varchar(255) NOT NULL,
  `upi_no` varchar(255) NOT NULL,
  `blog_count` int(11) NOT NULL DEFAULT 0,
  `comment` text NOT NULL,
  `blog_type` varchar(254) DEFAULT NULL,
  `price` bigint(20) DEFAULT NULL,
  `blog_data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`blog_data`)),
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `writers`
--

INSERT INTO `writers` (`writers_id`, `writers_name`, `email`, `password`, `phone`, `address`, `bank_name`, `account_no`, `acc_holdr_name`, `ifsc`, `upi_no`, `blog_count`, `comment`, `blog_type`, `price`, `blog_data`, `status`, `created_at`, `updated_at`) VALUES
(1, 'vaibhav sawant', 'vaibhavsawant@gmail.com', 'vaibhav@2001', '7887380238', 'SQLSTATE[HY000]: General error: 1364 Field \'assign_to\' doesn\'t have a default valueSQLSTATE[HY000]: General error: 1364 Field \'assign_to\' doesn\'t have a default value', 'vaibhav sawant', '1245789878654', '1245789878654', '1245789', '7887380238', 8, '', '', 0, '', 1, '2023-12-06 18:20:16', '2023-12-11 14:38:32'),
(2, 'vishal pawar', 'vishal@gmail.com', '', '7887380238', 'rfgfgfgfg', 'vaibhav sawant', '1245789878654', '1245789878654', '1245789', '7887380238', 3, '', '', 0, '', 1, '2023-12-06 18:27:26', '2023-12-11 13:52:13'),
(3, 'sharad', 'sharad@rfghrfh', '12345', '7887380238', 'sharadsharad', 'sharadsharadsharad', '1245789878654', '1245789878654', '1245789', '7887380238', 2, '', '', 0, '', 1, '2023-12-06 19:55:33', '2023-12-09 16:01:15'),
(4, 'vishal pawar', 'bgb@gmail.com', NULL, '7887380238', 'thth', 'btgbn', '1245789878654', '1245789878654', '1245789', '7887380238', 1, '', '', 0, '', 1, '2023-12-11 17:18:26', '2023-12-11 17:18:26'),
(5, 'vaibhav sawant0', 'jvhm', NULL, 'vhk', 'mhmhm', 'hmhm', 'jhnhn', '1245789878654', 'yjyj', 'yjyhj', 1, '', '', 0, '', 1, '2023-12-11 17:38:23', '2023-12-11 17:38:23'),
(6, 'tsgfvgdfvsd vcd', 'dfsdfsdvsfgvfgvrfv', 'dvcdvcdvcdv', 'dcvdsvsdfv', '2023-12-12', 'rfrf', 'rfrfg', 'erfrf', 'erferfg', 'efef', 0, 'vaibhav rgrrgr', 'erfr', 787558255, '', 1, '2023-12-11 18:15:06', '2023-12-11 18:15:06'),
(7, 'tsgfvgdfvsd vcd', 'dfsdfsdvsfgvfgvrfv', 'dvcdvcdvcdv', 'dcvdsvsdfv', '2023-12-12', 'rfrf', 'rfrfg', 'erfrf', 'erferfg', 'efef', 0, 'vaibhav rgrrgr', 'erfr', 787558255, '', 1, '2023-12-11 18:23:16', '2023-12-11 18:23:16'),
(8, 'tsgfvgdfvsd vcd', 'dfsdfsdvsfgvfgvrfv', 'dvcdvcdvcdv', 'dcvdsvsdfv', '2023-12-12', 'rfrf', 'rfrfg', 'erfrf', 'erferfg', 'efef', 0, 'vaibhav rgrrgr', 'erfr', 787558255, '', 1, '2023-12-11 18:23:45', '2023-12-11 18:23:45'),
(9, 'tsgfvgdfvsd vcd', 'dfsdfsdvsfgvfgvrfv', 'dvcdvcdvcdv', 'dcvdsvsdfv', '2023-12-12', 'rfrf', 'rfrfg', 'erfrf', 'erferfg', 'efef', 0, 'vaibhav rgrrgr', 'erfr', 787558255, '', 1, '2023-12-11 18:23:49', '2023-12-11 18:23:49'),
(10, 'vaibhav sawant', 'edeef@gmail.com', 'feff', '7887380238', 'vfvfv', 'sharadsharadsharad', '1245789878654', '1245789878654', '1245789', '7887380238', 0, 'vfvf', '1500', 2, '', 1, '2023-12-11 18:30:57', '2023-12-11 18:30:57'),
(11, 'tsgfvgdfvsd vcd', 'dfsdfsdvsfgvfgvrfv', 'dvcdvcdvcdv', 'dcvdsvsdfv', '2023-12-12', 'rfrf', 'rfrfg', 'erfrf', 'erferfg', 'efef', 0, 'vaibhav rgrrgr', 'erfr', 900, '', 1, '2023-12-11 19:25:19', '2023-12-11 19:25:19'),
(12, 'vaibhav sawant', 'v@gmail.com', 'thth', '7887380238', 'frgfgvfvfvbf', 'sharadsharadsharad', '1245789878654', 'rftdtdtgtg', '1245789', '7887380238', 0, '5yh5', '800', 23, '', 1, '2023-12-11 19:38:07', '2023-12-11 19:38:07'),
(13, 'vaibhav sawant', 'v@gmail.com', 'thth', '7887380238', 'frgfgvfvfvbf', 'sharadsharadsharad', '1245789878654', 'rftdtdtgtg', '1245789', '7887380238', 0, '5yh5', '1000', 2323, '', 1, '2023-12-11 19:38:07', '2023-12-11 19:38:07'),
(14, 'vaibhav sawant', 'v@gmail.com', 'thth', '7887380238', 'frgfgvfvfvbf', 'sharadsharadsharad', '1245789878654', 'rftdtdtgtg', '1245789', '7887380238', 0, '5yh5', '1500', 23232323, '', 1, '2023-12-11 19:38:07', '2023-12-11 19:38:07'),
(15, 'tstetetetetee', 'tstetetetetee@gmail.com', 'tstetetetetee', '7887380238', 'tstetetetetee', 'vaibhav sawant', '1245789878654', 'hnhn', '1245789', '7887380238', 0, '5yh5', '800', 8754, '', 1, '2023-12-11 19:56:42', '2023-12-11 19:56:42'),
(16, 'tstetetetetee', 'tstetetetetee@gmail.com', 'tstetetetetee', '7887380238', 'tstetetetetee', 'vaibhav sawant', '1245789878654', 'hnhn', '1245789', '7887380238', 0, '5yh5', '1000', 96, '', 1, '2023-12-11 19:56:42', '2023-12-11 19:56:42'),
(17, 'tstetetetetee', 'tstetetetetee@gmail.com', 'tstetetetetee', '7887380238', 'tstetetetetee', 'vaibhav sawant', '1245789878654', 'hnhn', '1245789', '7887380238', 0, '5yh5', '1500', 2, '', 1, '2023-12-11 19:56:42', '2023-12-11 19:56:42'),
(18, 'yjnhn', 'vv@gmail.com', 'thth', '7887380238', 'vdsgfghdfhd', 'vaibhav sawant', '1245789878654', 'hnhn', '1245789', '7887380238', 0, '5yh5', '800', 8754, '', 1, '2023-12-11 19:59:48', '2023-12-11 19:59:48'),
(19, 'gujmjhm', 'fff@gmail.com', 'thth', '7887380238', 'scsdcsd', 'vaibhav sawant', '1245789878654', '1245789878654', '1245789', '7887380238', 0, '5yh5', '800', 333, '', 1, '2023-12-11 20:03:49', '2023-12-11 20:03:49'),
(20, 'tsgfvgdfvsd vcd', 'dfsdfsdvsfgvfgvrfv', 'dvcdvcdvcdv', 'dcvdsvsdfv', '2023-12-12', 'rfrf', 'rfrfg', 'erfrf', 'erferfg', 'efef', 0, 'vaibhav rgrrgr', 'erfr', 900, '', 1, '2023-12-11 20:19:26', '2023-12-11 20:19:26'),
(21, 'vishal pawar', 'vhdh@gmail.com', 'thth', '7887380238', 'scsdcdc', 'vaibhav sawant', '1245789878654', '1245789878654', '1245789', '7887380238', 0, '5yh5', NULL, NULL, '{\"blog_type\":[\"800\",\"1000\",\"1500\",\"2000\"],\"price\":[\"8754\",\"5555\",\"4444\",\"3333\"]}', 1, '2023-12-11 20:30:01', '2023-12-11 20:30:01'),
(22, 'vishal pawar', 'efedfcedfc@gmail.com', 'thth', '7887380238', 'scsdcdc', 'vaibhav sawant', '1245789878654', '1245789878654', '1245789', '7887380238', 0, '5yh5', NULL, NULL, '{\"blog_type\":\"800,1000,1500,2000\",\"price\":\"8754,5555,4444,3333\"}', 1, '2023-12-11 20:50:56', '2023-12-11 20:50:56'),
(23, 'vishal pawar', 'efedfcedfc@gmail.com', 'thth', '7887380238', 'scsdcdc', 'vaibhav sawant', '1245789878654', '1245789878654', '1245789', '7887380238', 0, '5yh5', NULL, NULL, '{\"blog_type\":\"800{1000{1500{2000\",\"price\":\"8754{5555{4444{3333\"}', 1, '2023-12-11 20:57:06', '2023-12-11 20:57:06'),
(24, 'rrrrr', 'rrrrr@gmail.com', 'thth', '7887380238', 'fvbfbvrtfb', 'sharadsharadsharad', '1245789878654', 'hnhn', '1245789', '7887380238', 0, '5yh5', NULL, NULL, '[{\"blog_type\":\"800\",\"price\":\"777\"},{\"blog_type\":\"1000\",\"price\":\"888\"},{\"blog_type\":\"1500\",\"price\":\"999\"},{\"blog_type\":\"2000\",\"price\":\"1212\"}]', 1, '2023-12-11 21:29:07', '2023-12-11 21:29:07'),
(25, 'rrrrr', 'rrrrr@gmail.com', 'thth', '7887380238', 'fvbfbvrtfb', 'sharadsharadsharad', '1245789878654', 'hnhn', '1245789', '7887380238', 0, '5yh5', NULL, NULL, '[{\"blog_type\":\"800\",\"price\":\"777\"},{\"blog_type\":\"1000\",\"price\":\"888\"},{\"blog_type\":\"1500\",\"price\":\"999\"},{\"blog_type\":\"2000\",\"price\":\"1212\"}]', 1, '2023-12-11 21:38:43', '2023-12-11 21:38:43');

-- --------------------------------------------------------

--
-- Table structure for table `writers_categories`
--

CREATE TABLE `writers_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `writers_type` varchar(255) NOT NULL,
  `writers_price` bigint(20) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `writers_categories`
--

INSERT INTO `writers_categories` (`id`, `writers_type`, `writers_price`, `status`, `created_at`, `updated_at`) VALUES
(1, '800-1000', 250, '1', '2023-12-09 18:59:47', '2023-12-09 18:59:47'),
(2, '1000-1500', 350, '1', '2023-12-09 19:00:45', '2023-12-09 19:00:45'),
(3, '1500-2000', 500, '1', '2023-12-09 19:02:38', '2023-12-09 19:02:38');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`blog_id`),
  ADD KEY `blogs_writers_id_foreign` (`writers_id`);

--
-- Indexes for table `blog_categories`
--
ALTER TABLE `blog_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_writer`
--
ALTER TABLE `blog_writer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_reset_tokens`
--
ALTER TABLE `password_reset_tokens`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `subject_categories`
--
ALTER TABLE `subject_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `writers`
--
ALTER TABLE `writers`
  ADD PRIMARY KEY (`writers_id`);

--
-- Indexes for table `writers_categories`
--
ALTER TABLE `writers_categories`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `blog_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `blog_categories`
--
ALTER TABLE `blog_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `blog_writer`
--
ALTER TABLE `blog_writer`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `subject_categories`
--
ALTER TABLE `subject_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `writers`
--
ALTER TABLE `writers`
  MODIFY `writers_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `writers_categories`
--
ALTER TABLE `writers_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `blogs`
--
ALTER TABLE `blogs`
  ADD CONSTRAINT `blogs_writers_id_foreign` FOREIGN KEY (`writers_id`) REFERENCES `writers` (`writers_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
