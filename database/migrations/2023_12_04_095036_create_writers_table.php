<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('writers', function (Blueprint $table) {
            $table->bigInteger('writers_id');
            $table->string('writers_name');
            $table->string('email');
            $table->string('phone');
            $table->string('address');
            $table->string('bank_name');
            $table->string('account_no');
            $table->string('acc_holdr_name');
            $table->string('ifsc');
            $table->string('upi_no');
            $table->integer('blog_count')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('writers');
    }
};
