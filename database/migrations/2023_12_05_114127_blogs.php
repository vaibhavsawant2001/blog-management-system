<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('blogs', function (Blueprint $table) {
            $table->id(); // This will automatically create a bigIncrements column
            $table->bigInteger('writers_id')->unsigned();
            $table->string('subject');
            $table->string('topic');
            $table->string('keyword');
            $table->string('assign_to');
            $table->string('deadline');
            $table->string('blog_category');
            $table->string('comment');
            $table->timestamps();
    
            // Define foreign key constraint
            $table->foreign('writers_id')->references('writers_id')->on('writers');
        });
    }
    

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('blogs');
    }
};
