<?php
date_default_timezone_set("Asia/Kolkata");

$servername = 'localhost';
$serverusername = 'root';
$serverpassword = '';
$dbname = 'nag';

$mysqli = new mysqli($servername, $serverusername, $serverpassword, $dbname);

// Check for a connection error and use the correct format for displaying the error message
if ($mysqli->connect_error) {
    die("Connection failed: " . $mysqli->connect_error);
}

class Database{
    public $servername = 'localhost';
    public $serverusername = 'root';
    public $serverpassword = '';
    public $dbname = 'nag';
    public $conn;

    public function __construct()
    {
        $this->conn = new mysqli($this->servername, $this->serverusername, $this->serverpassword, $this->dbname);

        // Check for a connection error within the constructor
        if ($this->conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }
    }
}
?>

