<?php
include_once('header.php');
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Edit SEO
    </h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="seo.php">SEO</a></li>
      <li class="active">Edit SEO</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <h1>SEO</h1>
          <form enctype="multipart/form-data" action="/index.php" method="POST">
            <div class="box-body">
              <input type="hidden" class="form-control" name="id" value="8">
              <div class="row">
                <div class="form-group col-md-6">
                  <label for=" Name"> Title : </label>
                  <input type="text" class="form-control" name="title" readonly value="Home">
                </div>
                <div class="form-group col-md-6">
                  <label for=" Name"> Page Title : </label>
                  <input type="text" class="form-control" name="subject" required value="Home - Elettro Electrical Cabinet Accessories">
                </div>
              </div>
              <div class="form-group col-md-12">
                <h4 style="font-weight:bold;">Only For SEO Experts</h4><br>
                <label for="Contant"> Content : [Add meta tag ]</label>
                <textarea class="form-control" rows="4" name="content">
                  <!-- Your meta tags go here -->
                </textarea>
              </div>
              <div class="box-footer text-center">
                <button type="submit" name="submit" value="submit" class="btn btn-primary">Submit</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
</div>

<?php
include('includes/conn.php');

if (isset($_POST['submit'])) {
    // Retrieve data from the form
    $id = $_POST['id'];
    $title = $_POST['title'];
    $subject = $_POST['subject'];
    $content = $_POST['content'];

    // Define your SQL query with placeholders
    $sql = "INSERT INTO seo (id, title, subject, content) VALUES (?, ?, ?, ?)";
    $stmt = $connection->prepare($sql);

    // Bind parameters and their types
    $stmt->bind_param("iss", $id, $title, $subject, $content);

    if ($stmt->execute()) {
        echo "Data inserted successfully.";
    } else {
        echo "Error: " . $stmt->error;
    }

    // Close the statement and the database connection
    $stmt->close();
    $connection->close();
}
?>

<?php include('footer.php'); ?>





<?php
// Database configuration
$db_host = 'your_db_host';
$db_user = 'your_db_user';
$db_password = 'your_db_password';
$db_name = 'your_database_name';

// Create a database connection
$conn = mysqli_connect($db_host, $db_user, $db_password, $db_name);

// Check the connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

// Handle form submission
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $title = $_POST['title'];
    $page_title = $_POST['page_title'];
    $content = $_POST['content'];
    $status = $_POST['status'];

    // Insert data into the database
    $sql = "INSERT INTO seo_data (title, page_title, content, status) VALUES ('$title', '$page_title', '$content', '$status')";

    if (mysqli_query($conn, $sql)) {
        echo "Data inserted successfully.";
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }
}

// Close the database connection
mysqli_close($conn);
?>



<form method="post" action="insert_seo_data.php">
    <label for="title">Title:</label>
    <input type="text" name="title" id="title" required>
    <br>
    <label for="page_title">Page Title:</label>
    <input type="text" name="page_title" id="page_title" required>
    <br>
    <label for="content">Content:</label>
    <textarea name="content" id="content" rows="4" cols="100" required></textarea>
    <br>
    <label for="status">Status:</label>
    <input type="text" name="status" id="status" required>
    <br>
    <input type="submit" value="Submit">
</form>












<?php
// Check if the form has been submitted
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Database configuration
    $db_host = 'your_db_host';
    $db_user = 'your_db_user';
    $db_password = 'your_db_password';
    $db_name = 'your_database_name';

    // Create a database connection
    $conn = mysqli_connect($db_host, $db_user, $db_password, $db_name);

    // Check the connection
    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }

    // Retrieve data from the form
    $title = $_POST['title'];
    $page_title = $_POST['page_title'];
    $content = $_POST['content'];
    $status = $_POST['status'];

    // Insert data into the database
    $sql = "INSERT INTO seo_data (title, page_title, content, status) VALUES ('$title', '$page_title', '$content', '$status')";

    if (mysqli_query($conn, $sql)) {
        echo "Data inserted successfully.";
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }

    // Close the database connection
    mysqli_close($conn);
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Insert SEO Data</title>
</head>
<body>
    <h1>Insert SEO Data</h1>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <label for="title">Title:</label>
        <input type="text" name="title" id="title" required><br>

        <label for="page_title">Page Title:</label>
        <input type="text" name="page_title" id="page_title" required><br>

        <label for="content">Content:</label>
        <textarea name="content" id="content" rows="4" cols="50" required></textarea><br>

        <label for="status">Status:</label>
        <input type="text" name="status" id="status" required><br>

        <input type="submit" value="Submit">
    </form>
</body>
</html>



<?php
include_once('header.php');
include('includes/conn.php');

if (isset($_POST['submit'])) {
    $title = $_POST['title'];
    $subject = $_POST['subject'];
    $content = $_POST['content'];

    $sql = "INSERT INTO `seo` (`title`, `page_title`, `metatag`, `date`, `status`) VALUES (?, ?, ?, NOW(), 1)";
    $stmt = $connection->prepare($sql);

    if ($stmt) {
        $stmt->bind_param("sss", $title, $subject, $content);

        if ($stmt->execute()) {
            echo "Data inserted successfully.";
        } else {
            echo "Error: " . $stmt->error;
        }

        $stmt->close();
    } else {
        echo "Error: " . $connection->error;
    }
}

$connection->close();
?>

<!-- Your HTML form goes here -->

<?php include('footer.php'); ?>





<form enctype="multipart/form-data" method="POST">
    <div class="box-body">
        <input type="hidden" class="form-control" name="id" value="8">
        <div class="row">
            <div class="form-group col-md-6">
                <label for=" Name"> Title : </label>
                <input type="text" class="form-control" name="title" value="Home" readonly>
            </div>
            <div class="form-group col-md-6">
                <label for=" Name"> Page Title : </label>
                <input type="text" class="form-control" name="subject" value="<?php echo $subject; ?>" required>
            </div>
            <div class="form-group col-md-12">
                <h4 style="font-weight:bold;">Only For SEO Experts</h4><br>
                <label for="Content">Content: [Add meta tag]</label>
                <textarea class="form-control" rows="4" name="content"><?php echo $content; ?></textarea>
            </div>
        </div>
        <div class="box-footer text-center">
            <button type="submit" name="submit" value="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
</form>

<?php
include('includes/conn.php');

if (isset($_POST['submit'])) {
    $title = $_POST['title'];
    $subject = $_POST['subject'];
    $content = $_POST['content'];

    $sql = "UPDATE `seo` SET `page_title` = ?, `metatag` = ? WHERE `title` = ?";
    $stmt = $connection->prepare($sql);

    if ($stmt) {
        $stmt->bind_param("sss", $subject, $content, $title);

        if ($stmt->execute()) {
            echo "Data updated successfully.";
        } else {
            echo "Error: " . $stmt->error;
        }

        $stmt->close();
    } else {
        echo "Error: " . $connection->error;
    }
}

// Fetch the current SEO data for the form
$title = "Home"; // Set the default title
$subject = "Default page title"; // Set a default subject
$content = "Default content"; // Set default content

// You should fetch the actual data from your database using a SELECT query here and assign them to $title, $subject, and $content.

$connection->close();
?>



<?php
// Database connection details
$servername = "your_server_name";
$username = "your_username";
$password = "your_password";
$dbname = "your_database_name";

// Create a connection to the database
$conn = new mysqli($servername, $username, $password, $dbname);

// Check the connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Retrieve data from the form
    $id = $_POST['id'];
    $title = $_POST['title'];
    $page_title = $_POST['subject'];
    $metatag = $_POST['content']; // Assuming this is the field where you want to store the meta tags.

    // You can add more fields here as needed

    // SQL query to insert data into the 'seo' table
    $sql = "INSERT INTO seo (id, title, page_title, metatag) VALUES ('$id', '$title', '$page_title', '$metatag')";

    if ($conn->query($sql) === TRUE) {
        echo "Data inserted successfully.";
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }

    // Close the database connection
    $conn->close();
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>SEO Form</title>
</head>
<body>
    <form enctype="multipart/form-data" method="POST">
        <!-- Your form inputs here -->

        <div class="box-footer text-center">
            <button type="submit" name="submit" value="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
</body>
</html>











<?php
// Assuming you have a database connection
$servername = "your_server_name";
$username = "your_username";
$password = "your_password";
$dbname = "your_database_name";

// Create a connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check the connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Check if the form was submitted
if (isset($_POST['submit'])) {
    // Retrieve values from the form
    $id = $_POST['id'];
    $title = $_POST['title'];
    $subject = $_POST['subject'];
    $content = $_POST['content'];

    // Prepare and execute the SQL query
    $sql = "INSERT INTO seo (id, title, subject, content) VALUES (?, ?, ?, ?)";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("isss", $id, $title, $subject, $content);

    if ($stmt->execute()) {
        echo "Data inserted successfully.";
    } else {
        echo "Error: " . $conn->error;
    }

    // Close the statement and the database connection
    $stmt->close();
    $conn->close();
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>SEO Form</title>
</head>
<body>
    <form enctype="multipart/form-data" method="POST">
        <div class="box-body">
            <input type="hidden" class="form-control" name="id" value="8">
            <div class="row">
                <div class="form-group col-md-6">
                    <label for="Name">Title:</label>
                    <input type="text" class="form-control" name="title" value="Home" readonly>
                </div>
                <div class="form-group col-md-6">
                    <label for="Name">Page Title:</label>
                    <input type="text" class="form-control" name="subject" required>
                </div>
                <div class="form-group col-md-12">
                    <h4 style="font-weight:bold;">Only For SEO Experts</h4><br>
                    <label for="Content">Content: [Add meta tag]</label>
                    <textarea class="form-control" rows="4" name="content"></textarea>
                </div>
            </div>
            <div class="box-footer text-center">
                <button type="submit" name="submit" value="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
</body>
</html>









<!-- for update -->


<?php
include('includes/conn.php');
$servername = 'localhost';
$serverusername = 'root';
$serverpassword = '';
$dbname = 'nag';
$conn = new mysqli($servername, $serverusername, $serverpassword, $dbname);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

if (isset($_POST['submit'])) {
    $id = $_POST['id'];
    $title = $_POST['title'];
    $page_title = $_POST['page_title'];
    $metatag = $_POST['metatag'];

    $sql = "UPDATE seo SET title=?, page_title=?, metatag=? WHERE id=?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("sssi", $title, $page_title, $metatag, $id);

    if ($stmt->execute()) {
        echo "<script>alert('Data updated successfully.')</script>";
        header("Location: seo.php");
    } else {
        echo "Error: " . $conn->error;
    }
    $stmt->close();
}

if (isset($_GET['data'])) {
    $receivedData = $_GET['data'];
    $sql = "SELECT * FROM seo WHERE id = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("i", $receivedData);
    $stmt->execute();
    $result = $stmt->get_result();
    $data = $result->fetch_assoc();
    $stmt->close();
} else {
    echo "No data received.";
}

?>
<?php include_once('header.php');?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Edit SEO
        </h1>
        <ol class="breadcrumb">
            <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="seo.php"> SEO</a></li>
            <li class="active">Edit SEO</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <h1>SEO</h1>
                    <form enctype="multipart/form-data" method="POST">
                        <div class="box-body">
                            <input type="hidden" class="form-control" name="id" value="<?php echo $data['id']; ?>">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="Name">Title :</label>
                                    <input type="text" class="form-control" name="title" value="<?php echo $data['title']; ?>">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="Name">Page Title:</label>
                                    <input type="text" class="form-control" name="page_title" value="<?php echo $data['page_title']; ?>" required>
                                </div>
                                <div class="form-group col-md-12">
                                    <h4 style="font-weight:bold;">Only For SEO Experts</h4><br>
                                    <label for="Content">Content: [Add meta tag]</label>
                                    <textarea class="form-control" rows="4" name="metatag"><?php echo $data['metatag']; ?></textarea>
                                </div>
                            </div>
                            <div class="box-footer text-center">
                                <button type="submit" name="submit" value="submit" class="btn btn-primary">Update</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
<?php include('footer.php'); ?>










<?php 
$pageUrl = "your_page_url";

$query = "SELECT metatag FROM seo WHERE page_url = ?";
$stmt = mysqli_prepare($connection, $query);
mysqli_stmt_bind_param($stmt, "s", $pageUrl);
mysqli_stmt_execute($stmt);
mysqli_stmt_bind_result($stmt, $metaTitle, $metaDescription, $metaKeywords);
mysqli_stmt_fetch($stmt);
mysqli_stmt_close($stmt);
?>







<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "nag";
$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Define the $current_page variable (you should set it based on your requirements)
$current_page = "index.php";

// Build the SQL query with a JOIN and a WHERE clause
$sql = "SELECT seo.metatag
        FROM seo
        INNER JOIN site_pages ON seo.id = site_pages.page_id
        WHERE site_pages.page_url = '$current_page'";

$result = $conn->query($sql);

if ($result->num_rows > 0) {
    $row = $result->fetch_assoc();
    echo $row["metatag"]; // Assuming the column name is "meta_tags" in the database
} else {
    echo "No data found in the database.";
}

$conn->close();
?>












<?php



include_once('header.php');


// for edit
if (isset($_REQUEST['pro'])) {
  $edit_id = strip_tags($_REQUEST['pro']);
  $where = array('id' => $edit_id);
  if ($others = $model->select('product', $where)) {
      foreach ($others as $other) {
          $id = $other['id'];
          $image = $other['image'];
          $image2 = $other['image2'];
          $image3 = $other['image3'];
          $image4 = $other['image4'];
          $image5 = $other['image5'];
          $image6 = $other['image6'];
          $title = $other['name'];
          $s_desc = $other['s_desc'];
          $l_desc = $other['l_desc'];
          $category_id = $other['category'];
          $subcategory_id = $other['sub_category'];
          $price = $other['price'];
          $disc_price = $other['disc_price'];
          $title = $other['title'];
          $page_title = $other['page_title'];
          $metatag = $other['metatag'];
      }
  }
}

  if (isset($_REQUEST['banner_del_id'])) {
    $where_array = array( 'id' => strip_tags($_REQUEST['banner_del_id'])  );
    $stmt_del = $model->select('product',$where_array);
    foreach($stmt_del as $delete_image){
      if(!empty($delete_image['image2'])){
        $deleteimage = '../'.$delete_image['image2'];
        unlink($deleteimage);
      }
    }

    $update_array = array(
    'image2' => $delete_image,
  );
    if($model->update("product", $update_array, $where_array)){
      $model->url('product_details.php?succ=Update');
    }
    
}
  if (isset($_REQUEST['banner_del_id1'])) {
    $where_array = array( 'id' => strip_tags($_REQUEST['banner_del_id1'])  );
    $stmt_del = $model->select('product',$where_array);
    foreach($stmt_del as $delete_image){
      if(!empty($delete_image['image3'])){
        $deleteimage = '../'.$delete_image['image3'];
        unlink($deleteimage);
      }
    }

    $update_array = array(
    'image3' => $delete_image,
  );
    if($model->update("product", $update_array, $where_array)){
      $model->url('product_details.php?succ=Update');
    }
    
}

if (isset($_REQUEST['banner_del_id2'])) {
    $where_array = array( 'id' => strip_tags($_REQUEST['banner_del_id2'])  );
    $stmt_del = $model->select('product',$where_array);
    foreach($stmt_del as $delete_image){
      if(!empty($delete_image['image4'])){
        $deleteimage = '../'.$delete_image['image4'];
        unlink($deleteimage);
      }
    }

    $update_array = array(
    'image4' => $delete_image,
  );
    if($model->update("product", $update_array, $where_array)){
      $model->url('product_details.php?succ=Update');
    }
    
}
 
 if (isset($_REQUEST['banner_del_id3'])) {
    $where_array = array( 'id' => strip_tags($_REQUEST['banner_del_id3'])  );
    $stmt_del = $model->select('product',$where_array);
    foreach($stmt_del as $delete_image){
      if(!empty($delete_image['image5'])){
        $deleteimage = '../'.$delete_image['image5'];
        unlink($deleteimage);
      }
    }

    $update_array = array(
    'image5' => $delete_image,
  );
    if($model->update("product", $update_array, $where_array)){
      $model->url('product_details.php?succ=Update');
    }
    
}

if (isset($_REQUEST['banner_del_id4'])) {
    $where_array = array( 'id' => strip_tags($_REQUEST['banner_del_id4'])  );
    $stmt_del = $model->select('product',$where_array);
    foreach($stmt_del as $delete_image){
      if(!empty($delete_image['image6'])){
        $deleteimage = '../'.$delete_image['image6'];
        unlink($deleteimage);
      }
    }

    $update_array = array(
    'image6' => $delete_image,
  );
    if($model->update("product", $update_array, $where_array)){
      $model->url('product_details.php?succ=Update');
    }
    
}


if(isset($_POST['scat_edit'])){
  $blog_edit1 = 'scat_edit'; 
  $edit_id = $_POST['id'];

  $image1 = strip_tags($_POST['image1']);
  $image21 = $_FILES['image']['name'];
    if (empty($image21)) {
      $file = $image1;
    }
    else{  
      // for image replace
      $where = array( 'id' => $edit_id );
      $stmt_del = $model->select('product',$where);
      foreach($stmt_del as $delete_image){
        $deleteimage = '../'.$delete_image['image'];
        unlink($deleteimage);
      }

    $about_file = $_FILES['image']['name'];
    $target_dir1 = '../uploads/product/';
    $target_dir = 'uploads/product/';
    $newfilename = date('dmYHis').str_replace(" ", "", basename($about_file));
    $file1 = $target_dir1 . basename($newfilename);
    $file = $target_dir . basename($newfilename);
    $uploadOk = 1;
    $temp_file = $_FILES["image"]["tmp_name"];
  }

    $image2 = strip_tags($_POST['image2']);
    $image22 = $_FILES['image2']['name'];
    if (empty($image22)) {
      $file2 = $image2;
    }
    else{  
      // for image replace
      $where = array( 'id' => $edit_id );
      $stmt_del = $model->select('product',$where);
      foreach($stmt_del as $delete_image){
        $deleteimage = '../'.$delete_image['image2'];
        unlink($deleteimage);
      }

    $about_file2 = $_FILES['image2']['name'];
    $target_dir12 = '../uploads/product/';
    $target_dir2 = 'uploads/product/';
    $newfilename2 = date('dmYHis').str_replace(" ", "", basename($about_file2));
    $file12 = $target_dir12 . basename($newfilename2);
    $file2 = $target_dir2 . basename($newfilename2);
    $uploadOk2 = 1;
    $temp_file2 = $_FILES["image2"]["tmp_name"];
  } 

    $image3 = strip_tags($_POST['image3']);
    $image23 = $_FILES['image3']['name'];
    if (empty($image23)) {
      $file3 = $image3;
    }
    else{  
      // for image replace
      $where = array( 'id' => $edit_id );
      $stmt_del = $model->select('product',$where);
      foreach($stmt_del as $delete_image){
        $deleteimage = '../'.$delete_image['image3'];
        unlink($deleteimage);
      }

    $about_file3 = $_FILES['image3']['name'];
    $target_dir13 = '../uploads/product/';
    $target_dir3 = 'uploads/product/';
    $newfilename3 = date('dmYHis').str_replace(" ", "", basename($about_file3));
    $file13 = $target_dir13 . basename($newfilename3);
    $file3 = $target_dir3 . basename($newfilename3);
    $uploadOk3 = 1;
    $temp_file3 = $_FILES["image3"]["tmp_name"];
  } 

    $image4 = strip_tags($_POST['image4']);
    $image24 = $_FILES['image4']['name'];
    if (empty($image24)) {
      $file4 = $image4;
    }
    else{  
      // for image replace
      $where = array( 'id' => $edit_id );
      $stmt_del = $model->select('product',$where);
      foreach($stmt_del as $delete_image){
        $deleteimage = '../'.$delete_image['image4'];
        unlink($deleteimage);
      }

    $about_file4 = $_FILES['image4']['name'];
    $target_dir14 = '../uploads/product/';
    $target_dir4 = 'uploads/product/';
    $newfilename4 = date('dmYHis').str_replace(" ", "", basename($about_file4));
    $file14 = $target_dir14 . basename($newfilename4);
    $file4 = $target_dir4 . basename($newfilename4);
    $uploadOk4 = 1;
    $temp_file4 = $_FILES["image4"]["tmp_name"];
  } 

    $image5 = strip_tags($_POST['image5']);
    $image25 = $_FILES['image5']['name'];
    if (empty($image25)) {
      $file5 = $image5;
    }
    else{  
      // for image replace
      $where = array( 'id' => $edit_id );
      $stmt_del = $model->select('product',$where);
      foreach($stmt_del as $delete_image){
        $deleteimage = '../'.$delete_image['image5'];
        unlink($deleteimage);
      }

    $about_file5 = $_FILES['image5']['name'];
    $target_dir15 = '../uploads/product/';
    $target_dir5 = 'uploads/product/';
    $newfilename5 = date('dmYHis').str_replace(" ", "", basename($about_file5));
    $file15 = $target_dir15 . basename($newfilename5);
    $file5 = $target_dir5 . basename($newfilename5);
    $uploadOk5 = 1;
    $temp_file5 = $_FILES["image5"]["tmp_name"];
  } 

    $image6 = strip_tags($_POST['image6']);
    $image26 = $_FILES['image6']['name'];
    if (empty($image26)) {
      $file6 = $image6;
    }
    else{  
      // for image replace
      $where = array( 'id' => $edit_id );
      $stmt_del = $model->select('product',$where);
      foreach($stmt_del as $delete_image){
        $deleteimage = '../'.$delete_image['image6'];
        unlink($deleteimage);
      }

    $about_file6 = $_FILES['image6']['name'];
    $target_dir16 = '../uploads/product/';
    $target_dir6 = 'uploads/product/';
    $newfilename6 = date('dmYHis').str_replace(" ", "", basename($about_file6));
    $file16 = $target_dir16 . basename($newfilename6);
    $file6 = $target_dir6 . basename($newfilename6);
    $uploadOk6 = 1;
    $temp_file6 = $_FILES["image6"]["tmp_name"];
  } 


  $where_other = array( 'id' => $edit_id );

    $image = $file;
    $image2 = $file2;
    $image3 = $file3;
    $image4 = $file4;
    $image5 = $file5;
    $image6 = $file6;
    $title = addslashes(strip_tags(htmlentities($_POST['title'])));
    $category = addslashes(strip_tags(htmlentities($_POST['gallery_id'])));
     $s_desc = addslashes($_POST['s_desc']);
     $l_desc = addslashes($_POST['l_desc']);
     $price = addslashes(strip_tags(htmlentities($_POST['price'])));
     $disc_price = addslashes(strip_tags(htmlentities($_POST['disc_price'])));
     $title = addslashes(strip_tags(htmlentities($_POST['title'])));
     $page_title = addslashes(strip_tags(htmlentities($_POST['page_title'])));
     $metatag = addslashes(strip_tags(htmlentities($_POST['metatag'])));


    // $date = $todayDate;

  $update_array = array(
    'image' => $file,
    'image2' => $file2,
    'image3' => $file3,
    'image4' => $file4,
    'image5' => $file5,
    'image6' => $file6,
    'category' => $category,
    'name' => $title,
    's_desc' => $s_desc,
    'l_desc' => $l_desc,
    'price' => $price,
    'disc_price' => $disc_price,
    'title'=>$title,
    'page_title'=>$page_title,
    'metatag'=>$metatag,
    // 'date' => $todayDate,
  );
  if($model->update("product", $update_array, $where_other)){
    move_uploaded_file($temp_file, $file1);
    move_uploaded_file($temp_file2, $file12);
    move_uploaded_file($temp_file3, $file13);
    move_uploaded_file($temp_file4, $file14);
    move_uploaded_file($temp_file5, $file15);
    move_uploaded_file($temp_file6, $file16);
    // move_uploaded_file($temped_file, $file3);
    $model->url('product_details.php?succ=Update');
  }else{
      $model->url('product_details_add.php?pro='.$edit_id);
  }
}

// for insert
if(isset($_POST['submit'])){


  $about_file = $_FILES['image']['name'];
    if(!empty($about_file)){
      $target_dir1 = '../uploads/product/';
      $target_dir = 'uploads/product/';
      $newfilename = date('dmYHis').str_replace(" ", "", basename($about_file));
      $file1 = $target_dir1 . basename($newfilename);
      $file = $target_dir . basename($newfilename);
      $uploadOk = 1;
      $temp_file = $_FILES["image"]["tmp_name"];
    }else{
      $file1 = '';
      $file ='';
    }
   
   $about_file1 = $_FILES['image2']['name'];
    if(!empty($about_file1)){
      $target_dir12 = '../uploads/product/';
      $target_dir2 = 'uploads/product/';
      $newfilename2 = date('dmYHis').str_replace(" ", "", basename($about_file1));
      $file12 = $target_dir12 . basename($newfilename2);
      $file2 = $target_dir2 . basename($newfilename2);
      $uploadOk2 = 1;
      $temp_file2 = $_FILES["image2"]["tmp_name"];
    }else{
      $file12 = '';
      $file2 ='';
    }

    $about_file3 = $_FILES['image3']['name'];
    if(!empty($about_file3)){
      $target_dir13 = '../uploads/product/';
      $target_dir3 = 'uploads/product/';
      $newfilename3 = date('dmYHis').str_replace(" ", "", basename($about_file3));
      $file13 = $target_dir13 . basename($newfilename3);
      $file3 = $target_dir3 . basename($newfilename3);
      $uploadOk3 = 1;
      $temp_file3 = $_FILES["image3"]["tmp_name"];
    }else{
      $file13 = '';
      $file3 ='';
    }

    $about_file4 = $_FILES['image4']['name'];
    if(!empty($about_file4)){
      $target_dir14= '../uploads/product/';
      $target_dir4 = 'uploads/product/';
      $newfilename4 = date('dmYHis').str_replace(" ", "", basename($about_file4));
      $file14 = $target_dir14 . basename($newfilename4);
      $file4 = $target_dir4 . basename($newfilename4);
      $uploadOk4 = 1;
      $temp_file4 = $_FILES["image4"]["tmp_name"];
    }else{
      $file14 = '';
      $file4 ='';
    }

   $about_file5 = $_FILES['image5']['name'];
    if(!empty($about_file5)){
      $target_dir15= '../uploads/product/';
      $target_dir5 = 'uploads/product/';
      $newfilename5 = date('dmYHis').str_replace(" ", "", basename($about_file5));
      $file15 = $target_dir15 . basename($newfilename5);
      $file5 = $target_dir5 . basename($newfilename5);
      $uploadOk5 = 1;
      $temp_file5 = $_FILES["image5"]["tmp_name"];
    }else{
      $file15 = '';
      $file5 ='';
    }

    $about_file6 = $_FILES['image6']['name'];
    if(!empty($about_file6)){
      $target_dir16= '../uploads/product/';
      $target_dir6 = 'uploads/product/';
      $newfilename6 = date('dmYHis').str_replace(" ", "", basename($about_file6));
      $file16 = $target_dir16 . basename($newfilename6);
      $file6 = $target_dir6 . basename($newfilename6);
      $uploadOk6 = 1;
      $temp_file6 = $_FILES["image6"]["tmp_name"];
    }else{
      $file16 = '';
      $file6 ='';
    } 

    $image = $file;
    $image2 = $file2;
    $image3 = $file3;
    $image4 = $file4;
    $image5 = $file5;
    $image6 = $file6;
    $category = addslashes(strip_tags(htmlentities($_POST['gallery_id'])));
    $title = addslashes(strip_tags(htmlentities($_POST['title'])));
   // $sub_category = addslashes(strip_tags(htmlentities($_POST['sub_category'])));    
    $s_desc = addslashes($_POST['s_desc']);
    $l_desc = addslashes($_POST['l_desc']);
    $price = addslashes(strip_tags(htmlentities($_POST['price'])));
    $disc_price = addslashes(strip_tags(htmlentities($_POST['disc_price'])));
    $title = addslashes(strip_tags(htmlentities($_POST['title'])));
    $page_title = addslashes(strip_tags(htmlentities($_POST['page_title'])));
    $metatag = addslashes(strip_tags(htmlentities($_POST['metatag'])));
    // $content = addslashes($_POST['content']);
    // $date = $todayDate;
    $status = '1';

  $insert_array = array(
      'image' => $file,
      'name' => $title,
      'image2' => $image2,
      'image3' => $image3,
      'image4' => $image4,
      'image5' => $image5,
      'image6' => $image6,
      'category' => $category,
     //'sub_category' => $sub_category,
      's_desc' => $s_desc,
      'l_desc' => $l_desc,
      'price' => $price,
      'disc_price' => $disc_price,
      'title'=>$title,
      'page_title'=>$page_title,
      'metatag'=>$metatag,
      'status' => '1'
  );
  if($model->insert("product",$insert_array)){
        
        move_uploaded_file($temp_file, $file1);
        move_uploaded_file($temp_file2, $file12);
        move_uploaded_file($temp_file3, $file13);
        move_uploaded_file($temp_file4, $file14);
        move_uploaded_file($temp_file5, $file15);
        move_uploaded_file($temp_file6, $file16);
        $model->url('product_details.php?succ');
  }
  else
      $msg="faild";


 


}  
  


  

?>

<script type="text/javascript" src="ckeditor/ckeditor.js">
</script>


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Add Subcategory
    </h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="blogs.php">Subcategory</a></li>
      <li class="active">Add Subcategory</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content"> 
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">

        <div class="box box-primary">

          <div class="box-header with-border">
            <?php if (isset($_REQUEST['fail'])) {
              echo '<div class="alert alert-danger alert-dismissible">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              Something Went Wrong....
              </div>';
            } ?>

          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form enctype="multipart/form-data" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="POST">
            <div class="box-body">
               <?php if(isset($_REQUEST['pro'])){ ?>
                    <input type="hidden" class="form-control" name="id" value="<?= $edit_id; ?>" >
                 <?php } ?>
              
               <div class="row">
                <div class="form-group col-md-6">
                  <label for=" Name"> Name : </label>
                  <input type="text" class="form-control" name="product_title" required value="<?php if(isset($_REQUEST['pro'])) { echo $title; } elseif(isset($_POST['name'])) { echo $_POST['name']; } else{ echo ''; } ?>">
                </div>

                <div class="form-group col-md-6">
                  <label for=" Name">Category : </label>
                    <select class="form-control" name="gallery_id">
                    <?php
                    $where_array=array(
                    'status' => '1','type'=>'main'
                    );
                       if($datas = $model->select("gallery",$where_array)){ 
                        echo '<option value="">Select Category</option>';
                       foreach($datas as $data){ 
                        $id = $data['id'];
                        $title = $data['title'];
                    ?>
                   <option value="<?= $id; ?>" <?php if($gallery_id==$id) {
                    echo 'selected';
                    }
                    else 
                    {
                      echo "";
                    }
                  ?>>  <?= $title; ?> </option>
                   <?php  
                      }
                    }
                    ?>
                  </select>
                </div>

                <!--<div class="form-group col-md-6">
                  <label for=" Name">Sub Category : </label>
                    <select class="form-control" name="sub_category">
                    <?php
                    $where_array=array(
                    'status' => '1');
                       if($datas = $model->select("gallery_sub",$where_array)){ 
                        echo '<option value="">Select Sub Category</option>';
                       foreach($datas as $data){ 
                        $id = $data['id'];
                        $title = $data['title'];
                    ?>
                   <option value="<?= $id; ?>" <?php if($gallery_sub_id==$id) {
                    echo 'selected';
                    }
                    else 
                    {
                      echo "";
                    }
                  ?>>  <?= $title; ?> </option>
                   <?php  
                      }
                    }
                    ?>
                  </select>
                </div>-->

                <!-- <div class="form-group col-md-6">
                  <label for=" Name"> Subject : </label>
                  <input type="text" class="form-control" name="subject" required value="<?php if(isset($_REQUEST['scat_id'])) { echo $subject; } elseif(isset($_POST['subject'])) { echo $_POST['subject']; } else{ echo ''; } ?>">
                </div>
 -->

              <div class="form-group col-md-6">
                  <label for=" Name"> Price : </label>
                  <input type="text" class="form-control" name="price" value="<?php if(isset($_REQUEST['pro'])) { echo $price; } elseif(isset($_POST['price'])) { echo $_POST['price']; } else{ echo ''; } ?>">

              </div>

                <div class="form-group col-md-6">
                  <label for=" Name"> Discount Price : </label>
                  <input type="text" class="form-control" name="disc_price" value="<?php if(isset($_REQUEST['pro'])) { echo $disc_price; } elseif(isset($_POST['disc_price'])) { echo $_POST['disc_price']; } else{ echo ''; } ?>">
                </div>


             <div class="form-group col-md-12">
                <label for="Contant"> Short Description : </label>
                <textarea class="form-control" rows="4" name="s_desc" id="editor"><?php if(isset($_REQUEST['pro'])) { echo $s_desc; } elseif(isset($_POST['s_desc'])) { echo $_POST['s_desc']; } else{ echo ''; } ?></textarea>
             </div> 

             <div class="form-group col-md-12">
                <label for="Contant"> Long Description : </label>
                <textarea class="form-control" rows="4" name="l_desc" id="desc2"><?php if(isset($_REQUEST['pro'])) { echo $l_desc; } elseif(isset($_POST['l_desc'])) { echo $_POST['l_desc']; } else{ echo ''; } ?></textarea>
             </div> 

              <div class="form-group col-md-6">
                <label for="exampleInputFile">Main Image :-(Compulsory Image)<span  style="color: #ea3232">(Width) 71px × (Height) 65px </span> </label> <br />
                  
               <?php if(isset($_REQUEST['pro'])){ ?> 
                  <input type="hidden" name="image1" value="<?= $image; ?>">
                  <img src="../<?= $image; ?>"  height="100" width="100px"/>
               <?php }?>

                <input type="file" name="image" size="12"  data-toggle="tooltip"  data-placement="top" title="For Better Result Use Width and Height as Mention Above" required>  
              </div>

              <div class="form-group col-md-6">
                <label for="exampleInputFile">Product Images 1 :-<span  style="color: #ea3232">(Width) 71px × (Height) 65px </span> </label> <br />
                  
               <?php if(isset($_REQUEST['pro'])){ ?> 
                  <input type="hidden" name="image2" value="<?= $image2; ?>">
                  <img src="../<?= $image2; ?>"  height="100" width="100px"/>
               <?php }?>

                <input type="file" name="image2" size="12"  data-toggle="tooltip"  data-placement="top" title="For Better Result Use Width and Height as Mention Above">  

                <a href="product_details_add.php?banner_del_id=<?= $edit_id ; ?>" onclick="return confirm('Are you sure you want to delete ?')"> <span class="label label-danger"> Delete </span> </a>
              </div>

              <div class="form-group col-md-6">
                <label for="exampleInputFile">Product Images 2 :-<span  style="color: #ea3232">(Width) 71px × (Height) 65px </span> </label> <br />
                  
               <?php if(isset($_REQUEST['pro'])){ ?> 
                  <input type="hidden" name="image3" value="<?= $image3; ?>">
                  <img src="../<?= $image3; ?>"  height="100" width="100px"/>
               <?php }?>

                <input type="file" name="image3" size="12"  data-toggle="tooltip"  data-placement="top" title="For Better Result Use Width and Height as Mention Above">  
                <a href="product_details_add.php?banner_del_id1=<?= $edit_id ; ?>" onclick="return confirm('Are you sure you want to delete ?')"> <span class="label label-danger"> Delete </span> </a>
              </div>

              <div class="form-group col-md-6">
                <label for="exampleInputFile">Product Images 3 :-<span  style="color: #ea3232">(Width) 71px × (Height) 65px </span> </label> <br />
                  
               <?php if(isset($_REQUEST['pro'])){ ?> 
                  <input type="hidden" name="image4" value="<?= $image4; ?>">
                  <img src="../<?= $image4; ?>"  height="100" width="100px"/>
               <?php }?>

                <input type="file" name="image4" size="12"  data-toggle="tooltip"  data-placement="top" title="For Better Result Use Width and Height as Mention Above">

                <a href="product_details_add.php?banner_del_id2=<?= $edit_id ; ?>" onclick="return confirm('Are you sure you want to delete ?')"> <span class="label label-danger"> Delete </span> </a>  
              </div>

              <div class="form-group col-md-6">
                <label for="exampleInputFile">Product Images 4 :-<span  style="color: #ea3232">(Width) 71px × (Height) 65px </span> </label> <br />
                  
               <?php if(isset($_REQUEST['pro'])){ ?> 
                  <input type="hidden" name="image5" value="<?= $image5; ?>">
                  <img src="../<?= $image5; ?>"  height="100" width="100px"/>
               <?php }?>

                <input type="file" name="image5" size="12"  data-toggle="tooltip"  data-placement="top" title="For Better Result Use Width and Height as Mention Above"> 

                <a href="product_details_add.php?banner_del_id3=<?= $edit_id ; ?>" onclick="return confirm('Are you sure you want to delete ?')"> <span class="label label-danger"> Delete </span> </a>  
              </div>

              <div class="form-group col-md-6">
                <label for="exampleInputFile">Product Images 5 :-<span  style="color: #ea3232">(Width) 71px × (Height) 65px </span> </label> <br />
                  
               <?php if(isset($_REQUEST['pro'])){ ?> 
                  <input type="hidden" name="image6" value="<?= $image6; ?>">
                  <img src="../<?= $image6; ?>"  height="100" width="100px"/>
               <?php }?>

                <input type="file" name="image6" size="12"  data-toggle="tooltip"  data-placement="top" title="For Better Result Use Width and Height as Mention Above">

                <a href="product_details_add.php?banner_del_id4=<?= $edit_id ; ?>" onclick="return confirm('Are you sure you want to delete ?')"> <span class="label label-danger"> Delete </span> </a>   
              </div>
<!-- 
              <div class="form-group col-md-12">
                <label for="exampleInputFile">File :- </label> <br />
                  
               <?php if(isset($_REQUEST['scat_id'])){ ?> 
                  <input type="hidden" name="file1" value="<?= $file; ?>">
                  
               <?php echo $file; }?>

                <input type="file" name="file" size="12"  data-toggle="tooltip"  data-placement="top" title="For Better Result Use Width and Height as Mention Above">  
              </div>  
 -->
            </div>
          <h1> SEO</h1>
    
          <form enctype="multipart/form-data" action="/yaaaro_pms/seo_add.php" method="POST">
          <div class="box-body">
                  <input type="hidden" class="form-control" name="id" value="<?php if (isset($_REQUEST['scat_id'])) {
                                                                                echo $id;
                                                                              } elseif (isset($_POST['id'])) {
                                                                                echo $_POST['id'];
                                                                              } else {
                                                                                echo '';
                                                                              } ?>">

                  <div class="row">
                    <div class="form-group col-md-6">
                      <label for=" Name"> Title : </label>
                      <input type="text" class="form-control" name="title" readonly value="Product">
                    </div>

                    <div class="form-group col-md-6">
                      <label for=" Name"> Page Title : </label>
                      <input type="text" class="form-control" name="page_title" required value="<?php if (isset($_REQUEST['scat_id'])) {
                                                                                                  echo $page_title;
                                                                                                } elseif (isset($_POST['page_title'])) {
                                                                                                  echo $_POST['page_title'];
                                                                                                } else {
                                                                                                  echo '';
                                                                                                } ?>">
                    </div>

                    <div class="form-group col-md-12">
                      <h4 style="font-weight:bold;">Only For SEO Experts</h4><br>
                      <label for="Contant"> Content : [Add meta tag ]</label>
                      <textarea class="form-control" rows="4" name="metatag"><?php if (isset($_REQUEST['scat_id'])) {
                                                                                echo $metatag;
                                                                              } elseif (isset($_POST['metatag'])) {
                                                                                echo $_POST['metatag'];
                                                                              } else {
                                                                                echo '';
                                                                              } ?></textarea>
                    </div>
                  </div>

                  <div class="box-footer" align="center">
                    <button type="submit" name="<?php if (isset($_REQUEST['scat_id'])) {
                                                  echo 'scat_edit';
                                                } elseif (isset($scat_edit1) == 'scat_edit') {
                                                  echo 'scat_id';
                                                } else {
                                                  echo 'submit';
                                                } ?>" value="submit" class="btn btn-primary ">Submit</button>
                  </div>
                </div>
          </form>
        </div>
      </div>
    </div>
  </section>
</div>


<script>
    CKEDITOR.replace( 'desc2', {
        height:300,
        filebrowserUploadUrl:'upload.php'
    } );
</script>

<?php include('footer.php'); ?>