@include('yaaaro_pms/head')
<script src="https://cdn.ckeditor.com/ckeditor5/46.0.1/classic/ckeditor.js"></script>
<div class="content-wrapper">
    <!-- Content Header -->
    <section class="content-header">
        <h1>Add Blogs</h1>
    </section>
    <section class="content">
        <div class="box">
            <form action="{{route('writers.store')}}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('POST')
                <div class="box-body">
                    <div class="form-group">
                        <label for="title">Subject:</label>
                        <textarea name="subject" class="form-control" value="" placeholder="Enter Subject"
                            required></textarea>
                    </div>
                    <div class="form-group">
                        <label for="title">Topic:</label>
                        <textarea name="topic" class="form-control" value="" placeholder="Enter Topic"
                            autocomplete="off"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="title">Keyword:</label>
                        <input type="phone" name="keyword" class="form-control" value="" placeholder="Enter Keyword"
                            required>
                    </div>
                    <div class="form-group">
                        <label>Assign to</label>
                        <select class="form-control" name="assign_to">
                            <option value="">Select Writer</option>
                            <option value="" ></option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="title">Deadline</label>
                        <input type="date" name="deadline" class="form-control" value="" placeholder="Enter Deadline"
                            required>
                    </div>
                    <div class="form-group">
                        <label>Words</label>
                        <select class="form-control" name="words">
                            <option value="">Select Words</option>
                            <option value="" >120 to 150</option>
                            <option value="" >160 to 250</option>
                            <option value="" >260 to 300</option>
                        </select>
                    </div>

                    <div class="box-footer" align="center">
                        <button type="submit" name="submit" value="submit" class="btn btn-primary">Submit</button>
                    </div>

                </div>
            </form>
        </div>
    </section>

</div>
<script src="{{url('css/ckeditor/ckeditor.js')}}"></script>
<script>
    CKEDITOR.replace('ed        1', {
        allowedCo    ent:    });
</script>
<script>
        ClassicEditor
        .create(document.query        ctor('textarea'))
              .catch(error => {
                 });
</script>
@include('yaaaro_pms/footer')