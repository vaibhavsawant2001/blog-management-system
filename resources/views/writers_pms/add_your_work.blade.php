@include('writers_pms/head')
<script src="https://cdn.ckeditor.com/ckeditor5/46.0.1/classic/ckeditor.js"></script>
<div class="content-wrapper">
    <!-- Content Header -->
    <section class="content-header">
        <h1>WRITE HERE</h1>
    </section>
    <section class="content">
        <div class="box">
            <form action="{{ route('writers.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('POST')
                <div class="box-body">
                    <div class="form-group">
                        <label for="title">Write Your Work Here</label>
                        <textarea class="form-control" id="editor" name="address" rows="3"
                            placeholder="Enter Address"></textarea>
                    </div>
                    <div class="box-footer" align="center">
                        <button type="submit" name="action" value="draft" class="btn btn-secondary">Save Draft</button>
                        <button type="submit" name="action" value="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </section>
</div>
<script src="{{ url('css/ckeditor/ckeditor.js') }}"></script>
<script>
    CKEDITOR.replace('editor', {
        allowedContent: true
    });
</script>
<script>
    ClassicEditor
        .create(document.querySelector('textarea'))
        .catch(error => {
            console.error(error);
        });
</script>
@include('writers_pms/footer')
