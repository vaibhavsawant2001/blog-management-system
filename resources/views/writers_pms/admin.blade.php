@include('writers_pms/head')
<div class="content-wrapper">
    <section class="content-header">
        <h1 class="text-center">Dashboard</h1>
        <ol class="breadcrumb">
            <li><a href="{{url('writers_pms/admin')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-body table-responsive no-padding">
                <table class="table table-bordered" id="about_table">
                    <thead>
                        <tr>
                            <th hidden>ID</th>
                            <th class="text-center">Assigned Blogs</th>
                            <th class="text-center">Your Uploads</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="text-center">
                            <td hidden></td>
                            <td>Assigned Blogs : 26585</td>
                            <td>Uploaded Files Here</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>

@include('writers_pms/footer')