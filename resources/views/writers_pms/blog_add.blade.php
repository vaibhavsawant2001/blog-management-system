@include('yaaaro_pms/head')
<script src="https://cdn.ckeditor.com/ckeditor5/46.0.1/classic/ckeditor.js"></script>
<script type="text/javascript" src="{{url('ckeditor/ckeditor.js')}}"></script>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Add Blogs
    </h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i>Home</a></li>
      <li><a href="blogs.php">Blogs</a></li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
          </div>
          <form enctype="multipart/form-data" action="{{ route('blog.store') }}" method="POST">
            <div class="box-body">
              <input type="hidden" class="form-control" name="id" value="">
              <div class="row">
                <div class="form-group col-md-6">
                  <label for=" Name"> Blog Title : </label>
                  <input type="text" class="form-control" name="blog_title" value="">
                </div>
                <div class="form-group col-md-12">
                  <label for="Contant"> Blog Long Description : </label>
                  <textarea class="form-control" rows="4" name="blog_long_description" id="editor"></textarea>
                </div>
                <div class="form-group col-md-12">
                  <label for="Contant"> Blog Short Description : </label>
                  <textarea class="form-control" rows="4" name="blog_short_description" id="editor1"></textarea>
                </div>
                <div class="form-group col-md-6">
                  <label for=" Name"> Date : </label>
                  <input type="date" class="form-control" name="date" value="">
                </div>
                <div class="form-group col-md-6">
                  <label for=" Name"> Time : </label>
                  <input type="time" class="form-control" name="time" value="">
                </div>
                <div class="form-group col-md-12">
                  <label for="exampleInputFile">Image :- <span style="color: #ea3232">(Width) 71px × (Height) 65px </span> </label> <br />
                  <input type="hidden" name="image1" value="">
                  <img src="" height="100" width="100px" />
                  <input type="file" name="blog_image" size="12" data-toggle="tooltip" data-placement="top" title="For Better Result Use Width and Height as Mention Above">
                </div>
              </div>
              <h1>
                SEO
              </h1>
              <form enctype="multipart/form-data" action="" method="POST">
                <div class="box-body">
                  <input type="hidden" class="form-control" name="id" value="">
                  <div class="row">
                    <div class="form-group col-md-4">
                      <label for=" Name"> Title : </label>
                      <input type="text" class="form-control" name="header_title" readonly value="Blog">
                    </div>
                    <div class="form-group col-md-4">
                      <label for=" Name"> Page Title : </label>
                      <input type="text" class="form-control" name="page_title" value="">
                    </div>
                    <div class="form-group col-md-4">
                      <label for=" Name"> #URL : </label>
                      <input type="text" class="form-control" name="url_extension" value="">
                    </div>
                    <div class="form-group col-md-12">
                      <h4 style="font-weight:bold;">Only For SEO Experts</h4><br>
                      <label for="Contant"> Content : [Add meta tag ]</label>
                      <textarea class="form-control" rows="4" name="metatag"></textarea>
                    </div>
                  </div>
                  <div class="box-footer" align="center">
                    <button type="submit" name="submit" value="submit" class="btn btn-primary ">Submit</button>
                  </div>
                </div>
              </form>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
</div>
<script>
CKEDITOR.replace('editor1', {
    allowedContent: true
});
</script>
<script>
  ClassicEditor
    .create(document.querySelector('textarea'))
    .catch(error => {
      console.error(error);
    });
</script>
@include('yaaaro_pms/footer')