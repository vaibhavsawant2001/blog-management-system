@include('writers_pms/head')
<script src="https://cdn.ckeditor.com/ckeditor5/46.0.1/classic/ckeditor.js"></script>
<link rel="stylesheet" href="{{url('css/upload.css')}}">
<div class="content-wrapper">
  <!-- Content Header -->
  <section class="content-header">
    <h1>Your Blogs</h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-body table-responsive no-padding">
            <table id="client_master" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th hidden> ID </th>
                  <th>Blogs</th>
                  <th>Status</th>
                  <th>Opration</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td hidden></td>
                  <td>eef</td>
                  <td>a</td>
                  <td>
                    <a href="{{url('writers_pms/add_your_work')}}"><span class="label label-success">Add Your Work</span></a>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@include('writers_pms/footer')