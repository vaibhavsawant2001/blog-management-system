@include('yaaaro_pms/head')
<div class="content-wrapper">
    <section class="content-header">
        <h1>Blogs</h1>
        <ol class="breadcrumb">
            <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">About Us</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <center>
                <a href="{{url('/yaaaro_pms/add_blogs')}}" style="margin:12px 12px;" class="btn btn-primary">Add
                    Blogs</a>
            </center>
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover" id="about_table">
                    <thead>
                        <tr>
                            <th hidden>ID</th>
                            <th>Subjects</th>
                            <th>Topics</th>
                            <th>Keywords</th>
                            <th>Assign To</th>
                            <th>Deadline</th>
                            <th>Words</th>
                            <th class='text-center'>Operation</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td hidden></td>
                            <td><a href=""></a></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><a href="" class="label label-success">Edit</a></td>
                            <td>
                                <form action="" method="POST"
                                    onsubmit="return confirm('Are you sure you want to delete this company?')">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="label label-danger">Delete</button>
                                </form>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>

@include('yaaaro_pms/footer')