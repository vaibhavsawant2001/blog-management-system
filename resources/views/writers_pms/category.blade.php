@include('yaaaro_pms/head')

<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Category
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ url('yaaaro_pms/admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Category</li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header text-center">
            <a href="{{ url('yaaaro_pms/category_add') }}" class="btn btn-primary">Add Category</a>
          </div>
          <div class="box-body table-responsive no-padding">
            <table id="category_table" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Category</th>
                  <th>Delete</th>
                  <th>Edit</th>
                </tr>
              </thead>
              <tbody>
                @foreach($gallery_categories as $category)
                <tr>
                  <td>{{$category->id}}</td>
                  <td>{{$category->cat_name}}</td>
                  <td>
                    <a href="{{ route('category.destroy', ['category' => $category->id]) }}" onclick="event.preventDefault(); deleteCategory({{ $category->id }});">
                      <span class="btn btn-danger">Delete Category</span>
                    </a>
                  </td>
                  <td>
                    <a href="{{ route('category.edit', $category->id) }}">
                      <span class="btn btn-success">Edit</span>
                    </a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

@include('yaaaro_pms/footer')

<script>
  function deleteCategory(categoryId) {
    if (confirm("Are you sure you want to delete this category?")) {
      var deleteUrl = '{{ route('category.destroy', ['category' => ':categoryId']) }}';
      deleteUrl = deleteUrl.replace(':categoryId', categoryId);

      $.ajax({
        type: 'DELETE',
        url: deleteUrl,
        data: {
          "_token": "{{ csrf_token() }}"
        },
        success: function(data) {
          // Handle success, e.g., remove the row from the table
          alert('Category deleted successfully!');
          location.reload(); // Reload the page or update the table as needed
        },
        error: function(xhr, status, error) {
          console.log('Error:', xhr.responseText);
          alert('Failed to delete category.');
        }
      });
    }
  }
</script>


