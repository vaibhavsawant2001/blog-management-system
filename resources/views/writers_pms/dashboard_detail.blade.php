@include('yaaaro_pms/head')
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Update Details
    </h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="tag.php"> Detail</a></li>
      <li class="active">Add Detail</li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
          </div>
          <form action="{{ route('dashboard.update', [$dashboard->id]) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
            <div class="box-body">
              <div class="row">
                <div class="col-md-6 form-group">
                  <label for="Offer Type"> Company Name : </label>
                  <input type="text" class="form-control" name="company_name" value="{{$dashboard->company_name}}">
                </div>
                <div class="col-md-6 form-group">
                  <label for="Offer Type"> Company CEO : </label>
                  <input type="text" class="form-control" name="company_ceo" value="{{$dashboard->company_ceo}}">
                </div>
              </div>
              <div class="row">
                <div class="form-group col-md-6">
                  <label for="Content"> Company Address : </label>
                  <textarea class="form-control" rows="4" name="company_address">{{$dashboard->company_address}}</textarea>
                </div>
                <div class="form-group col-md-6">
                  <label for="Content"> Company Email : </label>
                  <input type="email" class="form-control" name="company_email" value="{{$dashboard->company_email}}">
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 form-group">
                  <label for="Offer Type"> Company Contact : </label>
                  <input type="text" class="form-control" name="company_contact" value="{{$dashboard->company_contact}}">
                </div>
                <div class="col-md-6 form-group">
                  <label for="Offer Type"> Company Whatsapp Number : </label>
                  <input type="text" class="form-control" name="company_whatsapp_no" value="{{$dashboard->company_whatsapp_no}}">
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 form-group">
                  <label for="Offer Type"> Company Location : </label>
                  <input type="text" class="form-control" name="company_location" value="{{$dashboard->company_location}}">
                </div>
                <div class="col-md-6 form-group">
                  <label for="Offer Type"> Image : </label>
                  <input type="file" class="form-control" name="image" value="{{$dashboard->image}}">
                </div>
              </div>

              <div class="row">
                <div class="col-md-3 form-group">
                  <label for="Offer Type"> Facebook Link : </label>
                  <input type="text" class="form-control" name="facebooklink" value="{{$dashboard->facebooklink}}">
                </div>
                <div class="col-md-3 form-group">
                  <label for="Offer Type"> Twitter Link : </label>
                  <input type="text" class="form-control" name="twitterlink" value="{{$dashboard->twitterlink}}">
                </div>
                <div class="col-md-3 form-group">
                  <label for="Offer Type"> Instagram Link : </label>
                  <input type="text" class="form-control" name="instagramlink" value="{{$dashboard->instagramlink}}">
                </div>
                <div class="col-md-3 form-group">
                  <label for="Offer Type"> Linkedin Link : </label>
                  <input type="text" class="form-control" name="linkedinlink" value="{{$dashboard->linkedinlink}}">
                </div>
              </div>


              <div class="box-footer" align="center">
                <button type="submit" name="submit" value="submit" class="btn btn-primary">Submit</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
</div>
@include('yaaaro_pms/footer')