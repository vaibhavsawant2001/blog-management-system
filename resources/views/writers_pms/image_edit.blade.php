@include('yaaaro_pms/head')
<script type="text/javascript" src="ckeditor/ckeditor.js">
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            ADD IMAGE
        </h1>
        <ol class="breadcrumb">
            <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="blogs.php">Image</a></li>
            <li class="active">Add Image</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                    </div>
                    <form action="{{ route('gallery.update', [$gallery->id]) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="box-body">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for=" Name"> Image Title : </label>
                                    <input type="text" class="form-control" name="image_title" required value="{{$gallery->image_title}}">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="gallery_id">Image Category:</label>
                                    <select class="form-control" name="image_category">
                                        <option value="{{$gallery->image_category}}">{{$gallery->image_category}}</option>
                                        @foreach($galleryCategories as $galleryCategory)
                                        <option value="{{$galleryCategory->cat_name}}">{{$galleryCategory->cat_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label for=" Name"> Main Image : </label>
                                    <input type="file" class="form-control" name="image" value="{{$gallery->image1}}">
                                </div>
                            </div>
                        </div>
                        <div class="box-footer" align="center">
                            <button type="submit" name="" value="submit" class="btn btn-primary ">Submit</button>
                        </div>
                </div>
            </div>
            </form>
        </div>
</div>
</div>
</section>
</div>
<script>
    CKEDITOR.replace('desc2', {
        height: 300,
        filebrowserUploadUrl: 'upload.php'
    });
</script>

@include('yaaaro_pms/footer')