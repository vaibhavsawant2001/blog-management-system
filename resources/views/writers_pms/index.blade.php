<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Design by foolishdeveloper.com -->
    <title>Writers Login</title>
    <link rel="stylesheet" href="{{url('css/style.css')}}">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;500;600&display=swap" rel="stylesheet">
</head>

<body>
    <form action="{{route('writer.login')}}" method="post">
        <div class="containers">
            <h3>Writers Login</h3>
            <input type="text" placeholder="Email" name="email" id="username">
            <input type="password" placeholder="Password" name="password" id="password">
            <center><button type="submit">Login</button></center>
        </div>
    </form>
</body>
</html>