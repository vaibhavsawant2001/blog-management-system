
@include('yaaaro_pms/head')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Edit SEO
        </h1>
        <ol class="breadcrumb">
            <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="seo.php"> SEO</a></li>
            <li class="active">Edit SEO</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                    </div>
                    <h1>SEO</h1>
                    <form action="{{ route('seo.update', [$seo->id]) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
                        <div class="box-body">
                            <input type="hidden" class="form-control" name="id" value="">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="Name">Title :</label>
                                    <input type="text" class="form-control" name="header_title" readonly value="{{$seo->header_title}}">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="Name">Page Title:</label>
                                    <input type="text" class="form-control" name="page_title" value="{{$seo->page_title}}" required>
                                </div>
                                <div class="form-group col-md-12">
                                    <h4 style="font-weight:bold;">Only For SEO Experts</h4><br>
                                    <label for="Content">Content: [Add meta tag]</label>
                                    <textarea class="form-control" rows="4" name="metatag">{{$seo->metatag}}</textarea>
                                </div>
                            </div>
                            <div class="box-footer text-center">
                                <button type="submit" name="submit" value="submit" class="btn btn-primary">Update</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
@include('yaaaro_pms/footer')