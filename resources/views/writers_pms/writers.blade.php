@include('yaaaro_pms/head')
<div class="content-wrapper">
  <section class="content-header">
    <h1>Writers</h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">About Us</li>
    </ol>
  </section>
  <section class="content">
    <div class="box">
    <center>
          <a href="{{url('yaaaro_pms/add_writers')}}" style="margin:15px 15px;" class="btn btn-primary">Add Writers</a>
      </center>
      <div class="box-body table-responsive no-padding">
        <table class="table table-hover" id="about_table">
          <thead>
            <tr>
              <th hidden>ID</th>
              <th>Writer's Name</th>
              <th>Email Id</th>
              <th>Phone Number</th>
              <th>Address</th>
              <th>Bank Name</th>
              <th>Account Number</th>
              <th>Account Holder's Name</th>
              <th>IFSC Code</th>
              <th>UPI Number</th>
              <th>Operation</th>
            </tr>
          </thead>
          <tbody>
            @foreach($writer as $writer)
          <tr>
            <td hidden></td>
            <td><a href="{{url('/yaaaro_pms/writers_details')}}">{{$writer->writers_name}}</a></td>
            <td>{{$writer->email}}</td>
            <td>{{$writer->phone}}</td>
            <td>{{$writer->address}}</td>
            <td>{{$writer->bank_name}}</td>
            <td>{{$writer->account_no}}</td>
            <td>{{$writer->acc_holdr_name}}</td>
            <td>{{$writer->ifsc}}</td>
            <td>{{$writer->upi_no}}</td>
            <td><a href="{{ route('writers.edit', $writer->id) }}" class="label label-success">Edit</a></td>
            <td>
            <form action="{{ route('writers.destroy', $writer->id) }}" method="POST" onsubmit="return confirm('Are you sure you want to delete this company?')">
        @csrf
        @method('DELETE')
        <button type="submit" class="label label-danger">Delete</button>
    </form>
  </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </section>
</div>

@include('yaaaro_pms/footer')
