@include('yaaaro_pms/head')
<div class="content-wrapper">
    <section class="content-header">
        <h1>Writer Details</h1>
        <ol class="breadcrumb">
            <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">About Us</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover" id="about_table">
                    <thead>
                        <tr>
                            <th hidden>ID</th>
                            <th>Writers Name</th>
                            <th>Total Assigned Blogs</th>
                            <th>Blogs Completed</th>
                            <th>Blogs Pending</th>
                            <th>Payment</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td hidden></td>
                            <td>Sharad<a href=""></a></td>
                            <td>25</td>
                            <td>24</td>
                            <td>1</td>
                            <td>UPI Payment</td>
                            <td><a href="" class="label label-success">Review</a></td>
                            <td><a href="" class="label label-warning">Pending</a></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>

@include('yaaaro_pms/footer')