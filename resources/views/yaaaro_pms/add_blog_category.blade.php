@include('yaaaro_pms/head')
<script src="https://cdn.ckeditor.com/ckeditor5/46.0.1/classic/ckeditor.js"></script>
<div class="content-wrapper">
    <!-- Content Header -->
    <section class="content-header">
        <h1>Add Blogs Category</h1>
    </section>
    <section class="content">
        <div class="box">
            <form action="{{route('blogcategory.store')}}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('POST')
                <div class="box-body">
                    <div class="form-group">
                        <label for="title">Blog Category:</label>
                        <input type="text" name="blog_category" class="form-control" value="" required>
                    </div>
                    <div class="form-group">
                        <label for="title">Price:</label>
                        <input type="text" name="price" class="form-control" value="" required>
                    </div>
                    <div class="box-footer" align="center">
                        <button type="submit" name="submit" value="submit" class="btn btn-primary">Submit</button>
                    </div>

                </div>
            </form>
        </div>
    </section>

</div>
<script src="{{url('css/ckeditor/ckeditor.js')}}"></script>
@include('yaaaro_pms/footer')