@include('yaaaro_pms/head')
<script src="https://cdn.ckeditor.com/ckeditor5/46.0.1/classic/ckeditor.js"></script>
<div class="content-wrapper">
    <!-- Content Header -->
    <section class="content-header">
        <h1>Add Blogs</h1>
    </section>
    <section class="content">
        <div class="box">
            <form action="{{route('blogs.store')}}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('POST')
                <div class="box-body">
                    <div class="form-group">
                        <label>Blog Category</label>
                        <select class="form-control" name="blog_category">
                            <option value="">Select Category</option>
                            @foreach($blogCategory as $blogCategory)
                            <option value="{{$blogCategory->blog_category}}">{{$blogCategory->blog_category}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Subject</label>
                        <select class="form-control" name="subject">
                            <option value="">Select Subject</option>
                            @foreach($subject as $subject)
                            <option value="{{$subject->subject_name}}">{{$subject->subject_name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Assign to</label>
                        <select class="form-control" name="assign_to">
                            <option value="">Select Writer</option>
                            @foreach($writers as $writers)
                            <option value="{{$writers->writers_name}}">{{$writers->writers_name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="title">Topic:</label>
                        <textarea name="topic" class="form-control" value="" placeholder="Enter Topic" autocomplete="off"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="title">Keyword:</label>
                        <input type="phone" name="keyword" class="form-control" value="" placeholder="Enter Keyword" required>
                    </div>
                    <div class="form-group">
                        <label for="title">Deadline</label>
                        <input type="date" name="deadline" class="form-control" value="" placeholder="Enter Deadline" required>
                    </div>
                    <div class="form-group">
                        <label for="title">Comment : </label>
                        <textarea class="form-control" id="editor1" name="comment" rows="3" placeholder="Enter Address"></textarea>
                    </div>
                    <div class="box-footer" align="center">
                        <button type="submit" name="submit" value="submit" class="btn btn-primary">Submit</button>
                    </div>

                </div>
            </form>
        </div>
    </section>

</div>
<script src="{{url('css/ckeditor/ckeditor.js')}}"></script>
<script>
CKEDITOR.replace('editor1', {
    allowedContent: true
});
</script>
<script>
  ClassicEditor
    .create(document.querySelector('textarea'))
    .catch(error => {
      console.error(error);
    });
</script>
<script>
    function submitForm() {
        if (confirm('Are you sure you want to submit the form?')) {
            document.getElementById('ourworkaddForm').submit();
            alert('Form submitted successfully! Redirecting to index page...');
            window.location.href = "{{url('yaaaro_pms/our_work_list')}}";
        }
    }
</script>
@include('yaaaro_pms/footer')