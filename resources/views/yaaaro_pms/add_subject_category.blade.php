@include('yaaaro_pms/head')
<script src="https://cdn.ckeditor.com/ckeditor5/46.0.1/classic/ckeditor.js"></script>
<div class="content-wrapper">
    <!-- Content Header -->
    <section class="content-header">
        <h1>Add Subject Category</h1>
    </section>
    <section class="content">
        <div class="box">
            <form action="{{route('subject.store')}}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('POST')
                <div class="box-body">
                    <div class="form-group">
                        <label for="title">Subject Name:</label>
                        <input type="text" name="subject_name" class="form-control" value="" required>
                    </div>
                    <div class="box-footer" align="center">
                        <button type="submit" name="submit" value="submit" class="btn btn-primary">Submit</button>
                    </div>

                </div>
            </form>
        </div>
    </section>

</div>
<script src="{{url('css/ckeditor/ckeditor.js')}}"></script>
@include('yaaaro_pms/footer')