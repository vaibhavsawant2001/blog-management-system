@include('yaaaro_pms/head')
<script src="https://cdn.ckeditor.com/ckeditor5/46.0.1/classic/ckeditor.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css" integrity="sha512-DTOQO9RWCH3ppGqcWaEA1BIZOC6xxalwEsw9c2QQeAIftl+Vegovlnee1c9QX4TctnWMn13TZye+giMm8e2LwA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<div class="content-wrapper">
  <!-- Content Header -->
  <section class="content-header">
    <h1>Add Writers</h1>
  </section>
  <section class="content">
    <div class="box">
      <form action="{{route('writers.store')}}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('POST')
        <div class="box-body">
          <div class="form-group">
            <label for="title">Writer's Name:</label>
            <input type="text" name="writers_name" class="form-control" value="" placeholder="Writers's Name" required>
          </div>
          <div class="form-group">
            <label for="title">Email Id:</label>
            <input type="hidden" name="id" value="">
            <input type="text" name="email" class="form-control" value="" placeholder="Enter email Id" autocomplete="off" required>
          </div>
          <div class="form-group">
            <label for="title">Phone:</label>
            <input type="phone" name="phone" class="form-control" value="" placeholder="Enter phone number" required>
          </div>
          <div class="form-group">
            <label for="title">Comment:</label>
            <input type="phone" name="comment" class="form-control" value="" placeholder="Enter Comment" required>
          </div>
          <div class="form-group">
            <label for="title">Address:</label>
            <textarea class="form-control" id="exampleFormControlTextarea1" name="address" rows="3" placeholder="Enter Address"></textarea>
          </div>
          <div class="form-group">
            <label for="title">Password:</label>
            <input type="text" name="password" class="form-control" value="" placeholder="Enter Password" required>
          </div>
          <hr style="margin-top: 20px;margin-bottom: 20px; border: 0;border-top: 1px solid #080707;">
          <hr style="margin-top: 20px;margin-bottom: 20px; border: 0;border-top: 1px solid #080707;">
          <div class="form-group">
    @foreach($writer as $writer)
        <label class="control-label">Blog Type: {{ $writer->blog_category }}</label>
        <div class="input-group">
            <input name="blog_type[]" value="{{ $writer->blog_category }}" type="hidden">
            <input name="price[]" placeholder="price" class="form-control" type="text">
        </div>
    @endforeach
</div>



          <hr style="margin-top: 20px;margin-bottom: 20px; border: 0;border-top: 1px solid #080707;">
          <hr style="margin-top: 20px;margin-bottom: 20px; border: 0;border-top: 1px solid #080707;">
          <h3>Payment</h3>
          <div class="form-group">
            <label for="title">Bank Name</label>
            <input type="text" name="bank_name" class="form-control" value="" placeholder="Enter Bank Name" required>
          </div>
          <div class="form-group">
            <label for="title">Account Number</label>
            <input type="text" name="account_no" class="form-control" value="" placeholder="Enter Account Number" required>
          </div>
          <div class="form-group">
            <label for="title">Account Holder's Name</label>
            <input type="text" name="acc_holdr_name" class="form-control" value="" placeholder="Enter Account Holder's Name" required>
          </div>
          <div class="form-group">
            <label for="title">IFSC Code</label>
            <input type="text" name="ifsc" class="form-control" value="" placeholder="Enter IFSC Code" required>
          </div>

          <div class="form-group">
            <label for="title">UPI ID : </label><br>Google pay | Phone pay | Paytm
            <input type="text" name="upi_no" class="form-control" value="" placeholder="Enter UPI ID" required>
          </div>

          <div class="box-footer" align="center">
            <button type="submit" name="submit" value="submit" class="btn btn-primary">Submit</button>
          </div>
        </div>
      </form>
    </div>
  </section>
</div>
<script>
  $(document).ready(function() {
    $('#contact_form').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
          valid: 'glyphicon glyphicon-ok',
          invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
          first_name: {
            validators: {
              stringLength: {
                min: 2,
              },
              notEmpty: {
                message: 'Please supply your first name'
              }
            }
          },
          last_name: {
            validators: {
              stringLength: {
                min: 2,
              },
              notEmpty: {
                message: 'Please supply your last name'
              }
            }
          },
          email: {
            validators: {
              notEmpty: {
                message: 'Please supply your email address'
              },
              emailAddress: {
                message: 'Please supply a valid email address'
              }
            }
          },
          phone: {
            validators: {
              notEmpty: {
                message: 'Please supply your phone number'
              },
              phone: {
                country: 'US',
                message: 'Please supply a vaild phone number with area code'
              }
            }
          },
          address: {
            validators: {
              stringLength: {
                min: 8,
              },
              notEmpty: {
                message: 'Please supply your street address'
              }
            }
          },
          city: {
            validators: {
              stringLength: {
                min: 4,
              },
              notEmpty: {
                message: 'Please supply your city'
              }
            }
          },
          state: {
            validators: {
              notEmpty: {
                message: 'Please select your state'
              }
            }
          },
          zip: {
            validators: {
              notEmpty: {
                message: 'Please supply your zip code'
              },
              zipCode: {
                country: 'US',
                message: 'Please supply a vaild zip code'
              }
            }
          },
          comment: {
            validators: {
              stringLength: {
                min: 10,
                max: 200,
                message: 'Please enter at least 10 characters and no more than 200'
              },
              notEmpty: {
                message: 'Please supply a description of your project'
              }
            }
          }
        }
      })
      .on('success.form.bv', function(e) {
        $('#success_message').slideDown({
          opacity: "show"
        }, "slow") // Do something ...
        $('#contact_form').data('bootstrapValidator').resetForm();

        // Prevent form submission
        e.preventDefault();

        // Get the form instance
        var $form = $(e.target);

        // Get the BootstrapValidator instance
        var bv = $form.data('bootstrapValidator');

        // Use Ajax to submit form data
        $.post($form.attr('action'), $form.serialize(), function(result) {
          console.log(result);
        }, 'json');
      });
  });
</script>
<script>
  ClassicEditor
    .create(document.querySelector('textarea'))
    .catch(error => {
      console.error(error);
    });
</script>
@include('yaaaro_pms/footer')