@include('yaaaro_pms/head')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Add Banner
        </h1>
        <ol class="breadcrumb">
            <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="tag.php"> Banner</a></li>
            <li class="active">Add Banner</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                    </div>
                    <form action="{{ route('banner.update', [$banner->id]) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12 form-group">
                                    <label for="Offer Type"> Image Banner : </label>
                                    <input type="file" class="form-control" name="banner" value="{{$banner->image}}">
                                </div>
                            </div>
                            <div class="box-footer" align="center">
                                <button type="submit" name="submit" value="submit" class="btn btn-primary">Submit</button>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
@include('yaaaro_pms/footer')