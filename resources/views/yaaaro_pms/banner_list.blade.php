@include('yaaaro_pms/head')
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Banner
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Banner</li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header text-center">
            <a href="{{ url('yaaaro_pms/banner_add') }}" class="btn btn-primary">Add Banner</a>
          </div>
          <div class="box-body table-responsive no-padding">
            <table id="banner_table" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Image</th>
                  <th>status</th>
                  <th>Delete</th>
                </tr>
              </thead>
              <tbody>
                @foreach($bannerlist as $banner)
                <tr>
                  <td hidden>{{$banner->id}}</td>
                  <td><img src="{{ url('public/' . $banner->image) }}" height="100" width="100"></td>
                  <td>
                    <form method="POST" action="{{ route('update.status', ['id' => $banner->id]) }}">
                      @csrf
                      @method('POST')

                      @if($banner->status == 0)
                      <button type="submit" class="label label-danger">Denied</button>
                      @elseif($banner->status == 1)
                      <button type="submit" class="label label-success">Approved</button>
                      @endif
                    </form>
                  </td>
                  <td>
                  <a href="{{ route('api.banner.destroy', ['banner' => $banner->id]) }}" onclick="event.preventDefault(); deleteBanner({{ $banner->id }});">
                  <span class="label label-danger">Delete</span>
                </a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@include('yaaaro_pms/footer')
<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
<script>
function deleteBanner(bannerId) {
   if (confirm("Are you sure you want to delete this banner?")) {
      var deleteUrl = '{{ route('api.banner.destroy', ['banner' => ':bannerId']) }}';
      deleteUrl = deleteUrl.replace(':bannerId', bannerId);

      $.ajax({
         type: 'DELETE',
         url: deleteUrl,
         data: {
            "_token": "{{ csrf_token() }}"
         },
         success: function(data) {
            // Handle success, e.g., remove the row from the table
            alert('Banner deleted successfully!');
            location.reload(); // Reload the page or update the table as needed
         },
         error: function(xhr, status, error) {
            console.log('Error:', xhr.responseText);
            alert('Failed to delete banner.');
         }
      });
   }
}

</script>
