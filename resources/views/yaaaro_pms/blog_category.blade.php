<!-- resources/views/your-blogs-blade-file.blade.php -->

@include('yaaaro_pms/head')

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Blogs
        </h1>
        <ol class="breadcrumb">
            <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Blogs</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header text-center">
                        <a href="{{url('yaaaro_pms/add_blog_category')}}" class="btn btn-primary">Add Blog Category</a>
                    </div>

                    <div class="box-body table-responsive no-padding">
                        <table id="client_master" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th hidden>ID</th>
                                    <th>Name</th>
                                    <th>Status</th>
                                    <th>Edit</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($blogCategory as $blogCategory)
                                <tr>
                                    <td hidden></td>
                                    <td>{{$blogCategory->blog_category}}</td>
                                    <td>
                                        <form method="POST" action="">
                                            @csrf
                                            @method('POST')
                                            <button type="submit" class="label label-success">Approved</button>
                                        </form>
                                    </td>
                                    <td>
                                        <a href="" class="label label-warning"><span class="label label-warning">Edit</span></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@include('yaaaro_pms/footer')