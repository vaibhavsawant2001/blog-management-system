<!-- resources/views/your-blogs-blade-file.blade.php -->

@include('yaaaro_pms/head')

<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Blogs
    </h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Blogs</li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header text-center">
            <a href="{{url('yaaaro_pms/blog_add')}}" class="btn btn-primary">Add Blogs</a>
          </div>

          <div class="box-body table-responsive no-padding">
            <table id="blog_master" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th hidden>ID</th>
                  <th>Image</th>
                  <th>Subject</th>
                  <th>Content</th>
                  <th>Status</th>
                  <th>Edit</th>
                  <th>Delete</th>
                </tr>
              </thead>
              <tbody>
                @foreach($blogs as $blog)
                  <tr>
                    <td hidden>{{$blog->id}}</td>
                    <td><img style="height:100px; width: 200px;" src="{{ url('public/' . $blog->blog_image) }}"></td>
                    <td>{{$blog->blog_title}}</td>
                    <td>{{ \Illuminate\Support\Str::limit($blog->blog_short_description, $limit = 50, $end = '...') }}</td>
                    <td>
    <form method="POST" action="{{ route('blog.status', ['id' => $blog->id]) }}">
        @csrf
        @method('POST')

        @if($blog->status == 0)
            <button type="submit" class="label label-danger">Denied</button>
        @elseif($blog->status == 1)
            <button type="submit" class="label label-success">Approved</button>
        @endif
    </form>
</td>
                    <td>
                      <a href="{{ route('blog.edit', ['blog' => $blog->id]) }}" class="label label-warning"><span class="label label-warning">Edit</span></a>
                    </td>
                    <td>
                      <a href="{{ route('blog.destroy', ['blog' => $blog->id]) }}" onclick="event.preventDefault(); deleteBlog({{ $blog->id }});">
                        <span class="label label-danger">Delete</span>
                      </a>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

@include('yaaaro_pms/footer')

<script>
  function deleteBlog(blogId) {
    if (confirm("Are you sure you want to delete this blog?")) {
      var deleteUrl = '{{ route('blog.destroy', ['blog' => ':blogId']) }}';
      deleteUrl = deleteUrl.replace(':blogId', blogId);

      $.ajax({
        type: 'DELETE',
        url: deleteUrl,
        data: {
          "_token": "{{ csrf_token() }}"
        },
        success: function(data) {
          // Handle success, e.g., remove the row from the table
          alert('Blog deleted successfully!');
          location.reload(); // Reload the page or update the table as needed
        },
        error: function(xhr, status, error) {
          console.log('Error:', xhr.responseText);
          alert('Failed to delete blog.');
        }
      });
    }
  }
</script>