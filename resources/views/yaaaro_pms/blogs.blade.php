@include('yaaaro_pms/head')
<div class="content-wrapper">
    <section class="content-header">
        <h1>Blogs</h1>
        <ol class="breadcrumb">
            <li><a href="admin.php"><i class="fa fa-dashboard"></i>Home</a></li>
            <li class="active">About Us</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <center>
                <a href="{{url('/yaaaro_pms/add_blogs')}}" style="margin:12px 12px;" class="btn btn-primary">Add
                    Blogs</a>
            </center>
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover" id="about_table">
                    <thead>
                        <tr>
                            <th hidden>ID</th>
                            <th>Subjects</th>
                            <th>Assign To</th>
                            <th>Deadline</th>
                            <th>Blog Type</th>
                            <th>Status</th>
                            <th class='text-center'>Operation</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($blog as $singleBlog)
                        <tr>
                            <td hidden></td>
                            <td>{{$singleBlog->subject}}</td>
                            <td>{{$singleBlog->assign_to}}</td>
                            <td>{{$singleBlog->deadline}}</td>
                            <td>{{$singleBlog->blog_category}}</td>
                            <td><a href="{{ route('blogs.edit', $singleBlog->blog_id) }}" class="label label-success">Add Status</a></td>
                            <td><a href="{{ route('blogs.edit', $singleBlog->blog_id) }}" class="label label-success">Edit</a></td>
                            <td>
                                <form action="{{ route('blogs.destroy', $singleBlog->blog_id) }}" method="POST" onsubmit="return confirm('Are you sure you want to delete this blog?')">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="label label-danger">Delete</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>

@include('yaaaro_pms/footer')