@include('yaaaro_pms/head')
<script src="https://cdn.ckeditor.com/ckeditor5/46.0.1/classic/ckeditor.js"></script>
<div class="content-wrapper">
    <!-- Content Header -->
    <section class="content-header">
        <h1>Update Status</h1>
    </section>
    <section class="content">
        <div class="box">
            <form action="{{ route('status.update', $status->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="box-body">
                    <div class="form-group">
                        <label for="title">Title:</label>
                        <input type="text" name="title" class="form-control" value="{{ $status->title }}" required>
                    </div>
                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="status" value="1" {{ $status->status == 1 ? 'checked' : '' }}>
                                Give Permission To Writer
                            </label>
                        </div>
                    </div>
                    <div class="box-footer" align="center">
                        <button type="submit" name="submit" value="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>

        </div>
    </section>
</div>
<script src="{{url('css/ckeditor/ckeditor.js')}}"></script>
@include('yaaaro_pms/footer')