@include('yaaaro_pms/head')
<script src="https://cdn.ckeditor.com/ckeditor5/46.0.1/classic/ckeditor.js"></script>
<div class="content-wrapper">
  <!-- Content Header -->
  <section class="content-header">
    <h1>Edit Writers</h1>
  </section>
  <section class="content">
    <div class="box">
      <form action="{{route('writers.update', $writer->writers_id)}}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT') 
        <div class="box-body">
          <div class="form-group">
            <label for="title">Writer's Name:</label>
            <input type="text" name="writers_name" class="form-control" value="{{$writer->writers_name}}" placeholder="Writers's Name" required>
          </div>
          <div class="form-group">
            <label for="title">Email Id:</label>
            <input type="hidden" name="id" value="">
            <input type="text" name="email" class="form-control" value="{{$writer->email}}" placeholder="Enter email Id"
              autocomplete="off" required>
          </div>
          <div class="form-group">
            <label for="title">Phone:</label>
            <input type="phone" name="phone" class="form-control" value="{{$writer->phone}}" placeholder="Enter phone number" required>
          </div>
          <div class="form-group">
            <label for="title">Address:</label>
            <textarea class="form-control" id="exampleFormControlTextarea1" name="address" rows="3"
              placeholder="Enter Address">{{$writer->address}}</textarea>
          </div>

          <h3>Bank Details</h3>
        <div class="form-group">
          <label for="title">Bank Name</label>
          <input type="text" name="bank_name" class="form-control" value="{{$writer->bank_name}}" placeholder="Enter Bank Name" required>
        </div>
        <div class="form-group">
          <label for="title">Account Number</label>
          <input type="text" name="account_no" class="form-control" value="{{$writer->account_no}}" placeholder="Enter Account Number" required>
        </div>
        <div class="form-group">
          <label for="title">Account Holder's Name</label>
          <input type="text" name="acc_holdr_name" class="form-control" value="{{$writer->acc_holdr_name}}" placeholder="Enter Account Holder's Name" required>
        </div>
        <div class="form-group">
          <label for="title">IFSC Code</label>
          <input type="text" name="ifsc" class="form-control" value="{{$writer->ifsc}}" placeholder="Enter IFSC Code" required>
        </div>
        
        <div class="form-group">
          <label for="title">UPI Number</label>
          <input type="text" name="upi_no" class="form-control" value="{{$writer->upi_no}}" placeholder="Enter UPI Number" required>
        </div>  

          <div class="box-footer" align="center">
            <button type="submit" name="submit" value="submit" class="btn btn-primary">Update</button>
          </div>
        </div>
      </form>
    </div>
  </section>

</div>
<script src="{{url('css/ckeditor/ckeditor.js')}}"></script>
<script>
  CKEDITOR.replace('editor1', {
    allowedContent: true
  });
</script>
<script>
  ClassicEditor
    .create(document.querySelector('textarea'))
    .catch(error => {
      console.error(error);
    });
</script>
@include('yaaaro_pms/footer')