@include('yaaaro_pms/head')

<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Everything Else
    </h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Everything Else</li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header text-center">
            <a href="{{url('yaaaro_pms/everything_else_add')}}" class="btn btn-primary">Add Everything Else</a>
          </div>

          <div class="box-body table-responsive no-padding">
            <table id="everything_else_master" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th hidden>ID</th>
                  <th>Image</th>
                  <th>Title</th>
                  <th>Content</th>
                  <th>Status</th>
                  <th>Edit</th>
                  <th>Delete</th>
                </tr>
              </thead>
              <tbody>
                @foreach($everythingElseList as $everythingElseList)
                  <tr>
                    <td hidden>{{$everythingElseList->id}}</td>
                    <td><img style="height:100px; width: 200px;" src="{{ url('public/' . $everythingElseList->image1) }}"></td>
                    <td>{{$everythingElseList->title}}</td>
                    <td>{{$everythingElseList->content}}</td>
                    <td>
    <form method="POST" action="{{ route('everything_else.status', ['id' => $everythingElseList->id]) }}">
        @csrf
        @method('POST')

        @if($everythingElseList->status == 0)
            <button type="submit" class="label label-danger">Denied</button>
        @elseif($everythingElseList->status == 1)
            <button type="submit" class="label label-success">Approved</button>
        @endif
    </form>
</td>
                    <td>
                      <a href="{{ route('everything_else.edit', $everythingElseList->id) }}" class="label label-warning"><span class="label label-warning">Edit</span></a>
                    </td>
                    <td>
                      <a href="{{ route('everything_else.destroy', ['everything_else' => $everythingElseList->id]) }}" onclick="event.preventDefault(); deleteEverythingElse({{ $everythingElseList->id }});">
                        <span class="label label-danger">Delete</span>
                      </a>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

@include('yaaaro_pms/footer')

<script>
  function deleteEverythingElse(everythingElseId) {
    if (confirm("Are you sure you want to delete this entry?")) {
      var deleteUrl = '{{ route('everything_else.destroy', ['everything_else' => ':everythingElseId']) }}';
      deleteUrl = deleteUrl.replace(':everythingElseId', everythingElseId);

      $.ajax({
        type: 'DELETE',
        url: deleteUrl,
        data: {
          "_token": "{{ csrf_token() }}"
        },
        success: function(data) {
          // Handle success, e.g., remove the row from the table
          alert('Entry deleted successfully!');
          location.reload(); // Reload the page or update the table as needed
        },
        error: function(xhr, status, error) {
          console.log('Error:', xhr.responseText);
          alert('Failed to delete entry.');
        }
      });
    }
  }
</script>
