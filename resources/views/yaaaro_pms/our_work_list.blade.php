@include('yaaaro_pms/head')

<div class="content-wrapper">
  <section class="content-header">
    <h1>
      OUR WORK
    </h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Our Work</li>
    </ol>
  </section>
  <section class="content">
    <div class="box-header text-center">
      <a href="{{ url('yaaaro_pms/our_work_add') }}" class="btn btn-primary">Add Our Work</a>
    </div>
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-body table-responsive no-padding">
            <table id="our_work_table" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th hidden>ID</th>
                  <th>Image</th>
                  <th>Title</th>
                  <th>Short Description</th>
                  <th>Long Description</th>
                  <th>Date</th>
                  <th>Delete</th>
                  <th>Edit</th>
                </tr>
              </thead>
              <tbody>
                @foreach($our_work_list as $our_work)
                <tr>
                  <td hidden>{{$our_work->id}}</td>
                  <td><img src="{{ url('public/' . $our_work->image1) }}" height="100" width="100"></td>
                  <td>{{$our_work->title}}</td>
                  <td>{{$our_work->short_description}}</td>
                  <td>{{$our_work->long_description}}</td>
                  <td>{{$our_work->created_at}}</td>
                  <td>
    <form action="{{ route('our_work.destroy', $our_work->id) }}" method="POST" onsubmit="return confirm('Are you sure you want to delete this Our Work?')">
        @csrf
        @method('DELETE')
        <button type="submit" class="btn btn-danger">Delete</button>
    </form>
</td>
                  <td>
                    <a href="{{ route('our_work.edit', ['our_work' => $our_work->id]) }}">
                      <span class="label label-success">Edit</span>
                    </a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

@include('yaaaro_pms/footer')