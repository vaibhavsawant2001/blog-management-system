@include('yaaaro_pms/head')
<div class="content-wrapper">
    <section class="content-header">
        <h1>Pending Blogs</h1>
        <ol class="breadcrumb">
            <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">About Us</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-body table-responsive no-padding">
                <table id="client_master" class="table table-hover" id="about_table">
                    <thead>
                        <tr>
                            <th hidden>ID</th>
                            <th>Blogs Name</th>
                            <th>Blogs Pending</th>
                            <th>Payment</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($pendingBlogs as $blog)
                        <tr>
                            <td hidden></td>
                            <td>{{$blog->subject}}</td>
                            <td></td>
                            <td></td>
                            <td>
                                <a href="" class="label label-success">Review</a>
                                <a href="" class="label label-warning">Pending</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>
@include('yaaaro_pms/footer')