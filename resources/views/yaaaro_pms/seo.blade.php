@include('yaaaro_pms/head')

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Everything Else
        </h1>
        <ol class="breadcrumb">
            <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Everything Else</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body table-responsive no-padding">
                        <table id="everything_else_master" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Title</th>
                                    <th>Page Title</th>
                                    <th>Metatag</th>
                                    <th>Status</th>
                                    <th>Edit</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($seo as $seo)
                                <tr>
                                    <td>{{$seo->id}}</td>
                                    <td>{{$seo->header_title}}</td>
                                    <td>{{$seo->page_title}}</td>
                                    <td>{{$seo->metatag}}</td>
                                    <td>
                                        <form method="POST" action="{{ route('seo.status', ['id' => $seo->id]) }}">
                                            @csrf
                                            @method('POST')
                                            @if($seo->status == 0)
                                            <button type="submit" class="label label-danger">Denied</button>
                                            @elseif($seo->status == 1)
                                            <button type="submit" class="label label-success">Approved</button>
                                            @endif
                                        </form>
                                    </td>
                                    <td>
                                        <a href="{{ route('seo.edit', $seo->id) }}" class="label label-warning"><span class="label label-warning">Edit</span></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@include('yaaaro_pms/footer')