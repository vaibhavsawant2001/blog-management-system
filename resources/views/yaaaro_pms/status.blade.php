@include('yaaaro_pms/head')
<div class="content-wrapper">
    <section class="content-header">
        <h1>Status</h1>
        <ol class="breadcrumb">
            <li><a href="admin.php"><i class="fa fa-dashboard"></i>Home</a></li>
            <li class="active">Status</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <center>
                <a href="{{url('/yaaaro_pms/add_status')}}" style="margin:12px 12px;" class="btn btn-primary">Add
                    status</a>
            </center>
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover" id="client_master">
                    <thead>
                        <tr>
                            <th hidden>ID</th>
                            <th>Title</th>
                            <th>Writers Permission</th>
                            <th>Operation</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($status as $status)
                        <tr>
                            <td hidden></td>
                            <td>{{$status->title}}</td>
                            <td>
                                @if($status->status == 1)
                                <span class="label label-success">YES</span>
                                @else
                                <span class="label label-danger">NO</span>
                                @endif
                            </td>
                            <td><a href="{{ route('status.edit', $status->id) }}" class="label label-success">Edit</a></td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>
@include('yaaaro_pms/footer')