<!-- resources/views/your-blogs-blade-file.blade.php -->

@include('yaaaro_pms/head')

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Subject Category
        </h1>
        <ol class="breadcrumb">
            <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Blogs</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header text-center">
                        <a href="{{url('yaaaro_pms/add_subject_category')}}" class="btn btn-primary">Add Subject Category</a>
                    </div>

                    <div class="box-body table-responsive no-padding">
                        <table id="blog_master" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th hidden>ID</th>
                                    <th>Name</th>
                                    <th>Status</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($subject_category as $subject_category)
                                <tr>
                                    <td hidden></td>
                                    <td>{{$subject_category->subject_name}}</td>
                                    <td>
                                        <form method="POST" action="{{ route('subject.updateStatus', ['id' => $subject_category->id]) }}">
                                            @csrf
                                            @method('POST')
                                            @if($subject_category->status == 0)
                                            <button type="submit" class="label label-danger">Denied</button>
                                            @elseif($subject_category->status == 1)
                                            <button type="submit" class="label label-success">Approved</button>
                                            @endif
                                        </form>
                                    </td>
                                    <td>
                                        <a href="" class="label label-warning"><span class="label label-warning">Edit</span></a>
                                    </td>
                                    <td>
                                        <form method="POST" action="{{ route('subject.destroy', $subject_category->id) }}">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="label label-danger">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@include('yaaaro_pms/footer')