@include('yaaaro_pms/head')
<div class="content-wrapper">
  <section class="content-header">
    <h1>Writers</h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">About Us</li>
    </ol>
  </section>
  <section class="content">
    <div class="box">
      <center>
        <a href="{{url('yaaaro_pms/add_writers')}}" style="margin:15px 15px;" class="btn btn-primary">Add Writers</a>
      </center>
      <div class="box-body table-responsive no-padding">
        <table id="client_master" class="table table-hover" id="about_table">
          <thead>
            <tr>
              <th hidden>ID</th>
              <th>Writer's Name</th>
              <th>Email Id</th>
              <th>Total Assign Blog</th>
              <th>Pending Blogs</th>
              <th>Complete Blogs</th>
              <th>Operation</th>
            </tr>
          </thead>
          <tbody>
            @foreach($data['writer']->unique('writers_id') as $blog)
            <tr>
              <td hidden></td>
              <td><a href="{{ route('writers.show', $blog->writer->writers_id) }}">{{ $blog->writer->writers_name }}</a></td>
              <td>{{ $blog->writer->email }}</td>
              <td>{{ $blog->writer->phone }}</td>
              <?php
              $pendingCount = $blog->writer->blogs->where('status', 0)->count();
              $completeCount = $blog->writer->blogs->where('status', 1)->count();
              ?>
              <td><a href="{{ route('pending.blogs', $blog->writer->writers_id) }}">{{ $pendingCount }}</a></td>
              <td><a href="{{ route('completed.blogs', $blog->writer->writers_id) }}">{{ $completeCount }}</a></td>
              <td>
                <a href="{{ route('writers.edit', $blog->writer->writers_id) }}" class="label label-success">Edit</a>
                @if($blog->writer->status == 1)
                <a href="" class="label label-success">Active</a>
                @elseif($blog->writer->status == 0)
                <a href="" class="label label-danger">Deactive</a>
                @endif
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </section>
</div>
@include('yaaaro_pms/footer')