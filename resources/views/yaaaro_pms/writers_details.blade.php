@include('yaaaro_pms/head')
<div class="content-wrapper">
    <section class="content-header">
        <h1>Writer Details</h1>
        <ol class="breadcrumb">
            <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">About Us</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-body table-responsive no-padding">
                <table id="client_master" class="table table-hover" id="about_table">
                    <thead>
                        <tr>
                            <th hidden>ID</th>
                            <th>Writers Name</th>
                            <th>Total Assigned Blogs</th>
                            <th>Blogs Name</th>
                            <th>Blogs Pending</th>
                            <th>Payment</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($writer)
                        <tr>
                            <td hidden>{{ $writer->writers_id }}</td>
                            <td>{{ $writer->writers_name }}</td>
                            <td>{{ $writer->blog_count }}</td>
                            <td>
                                @php
                                $pendingCount = 0;
                                @endphp
                                @foreach($writer->blogs as $blog)
                                <div>
                                    {{ $blog->subject }} <br>
                                    <h3>Deadline: {{ $blog->deadline }}</h3>
                                    @if($blog->status == 0)
                                    <strong>Status:</strong> <span class="label label-warning">Pending</span>
                                    @php
                                    $pendingCount++;
                                    @endphp
                                    @elseif($blog->status == 1)
                                    <strong>Status:</strong> <span class="label label-success">Complete</span>
                                    @endif
                                    <hr>
                                </div>
                                @endforeach
                            </td>
                            <td>{{ $pendingCount }}</td>
                            <td>200</td>
                            <td>
                                <a href="" class="label label-success">Review</a>
                                <a href="" class="label label-warning">Pending</a>
                            </td>
                        </tr>
                        @else
                        <tr>
                            <td colspan="7">Writer not found</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>

@include('yaaaro_pms/footer')