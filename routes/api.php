<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\BlogCategoryController;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\StatusController;
use App\Http\Controllers\SubjectCategoryController;
use App\Http\Controllers\WritersCategoryController;
use App\Http\Controllers\WritersController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
//login\logout route start here
Route::post('/login', [AuthController::class, 'login'])->name('writers.login');
Route::post('/logout', [AuthController::class, 'logout'])->name('writers.logout');
//writers route start here
Route::get('/writers', [WritersController::class, 'index'])->name('writers.index');
Route::get('/writers/show/{id}', [WritersController::class, 'show'])->name('writers.show');
Route::post('/writers', [WritersController::class, 'store'])->name('writers.store');
Route::get('/writers/{id}', [WritersController::class, 'edit'])->name('writers.edit');
Route::put('/writers/{id}', [WritersController::class, 'update'])->name('writers.update');
Route::delete('/writers/{id}', [WritersController::class, 'destroy'])->name('writers.destroy');
//blog route start here
Route::get('/blogs', [BlogController::class, 'index']);
Route::get('/blogs/show/{id}', [BlogController::class, 'show'])->name('blogs.show');
Route::post('/blogs', [BlogController::class, 'store'])->name('blogs.store');
Route::get('/blogs/{id}', [BlogController::class, 'edit'])->name('blogs.edit');
Route::put('/blogs/{id}', [BlogController::class, 'update'])->name('blogs.update');
Route::delete('/blogs/{id}', [BlogController::class, 'destroy'])->name('blogs.destroy');
//login\logout route start here
Route::post('writers/login', [LoginController::class, 'login'])->name('writer.login');
Route::post('writers/logout', [LoginController::class, 'logout'])->name('writer.logout');
//subject category route start here
Route::get('/subject', [SubjectCategoryController::class, 'index']);
Route::get('/subject/show/{id}', [SubjectCategoryController::class, 'show'])->name('subject.show');
Route::post('/subject', [SubjectCategoryController::class, 'store'])->name('subject.store');
Route::get('/subject/{id}', [SubjectCategoryController::class, 'edit'])->name('subject.edit');
Route::put('/subject/{id}', [SubjectCategoryController::class, 'update'])->name('subject.update');
Route::delete('/subject/delete/{id}', [SubjectCategoryController::class, 'destroy'])->name('subject.destroy');
Route::post('/subject/update-status/{id}', [SubjectCategoryController::class, 'updateStatus'])->name('subject.updateStatus');
//Blog category route start here
Route::get('/blogcategory', [BlogCategoryController::class, 'index'])->name('blogcategory.index');
Route::get('/blogcategory/show/{id}', [BlogCategoryController::class, 'show'])->name('blogcategory.show');
Route::post('/blogcategory', [BlogCategoryController::class, 'store'])->name('blogcategory.store');
Route::get('/blogcategory/{id}', [BlogCategoryController::class, 'edit'])->name('blogcategory.edit');
Route::put('/blogcategory/{id}', [BlogCategoryController::class, 'update'])->name('blogcategory.update');
Route::delete('/blogcategory/{id}', [BlogCategoryController::class, 'destroy'])->name('blogcategory.destroy');

//pending blogs route start here
Route::get('/pending-blogs/{writer_id}', [BlogController::class, 'showPendingBlogs'])->name('pending.blogs');
Route::get('/completed-blogs/{writer_id}', [BlogController::class, 'showCompletedBlogs'])->name('completed.blogs');

//status route start here
Route::get('/status', [StatusController::class, 'index'])->name('status.index');
Route::post('/status', [StatusController::class, 'store'])->name('status.store');
Route::get('/status/{id}', [StatusController::class, 'edit'])->name('status.edit');
Route::PUT('/status/{id}', [StatusController::class, 'update'])->name('status.update');
