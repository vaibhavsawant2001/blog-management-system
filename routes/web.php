<?php

use App\Models\Blog;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('writers_pms/index');
});

// Route::get('/writers_pms/index', function () {
//     return view('writers_pms/index');
// });

Route::get('writers_pms/admin', function () {
    return view('writers_pms/admin');
});

Route::get('writers_pms/head', function () {
    return view('writers_pms/head');
});

Route::get('writers_pms/blog_detail', function () {
    return view('writers_pms/blog_detail');
});

Route::get('writers_pms/add_your_work', function () {
    return view('writers_pms/add_your_work');
});


//Yaaaro_pms Route start here
Route::get('yaaaro_pms', function () {
    return view('yaaaro_pms/index');
});

Route::get('/yaaaro_pms/admin', function () {
    return view('yaaaro_pms/admin');
});

Route::get('/yaaaro_pms/writers', function () {
    $writer = Blog::with('writer')->get();
    $data = compact('writer');
    return view('yaaaro_pms/writers', ['data' => $data]);
});

Route::get('/yaaaro_pms/add_writers', function () {
    $writer = DB::table('blog_categories')->get();
    return view('yaaaro_pms/add_writers', ['writer' => $writer]);
});

Route::get('/yaaaro_pms/add_image', function () {
    return view('yaaaro_pms/add_image');
});

Route::get('/yaaaro_pms/blogs', function () {
    $blog = DB::table('blogs')->get();
    return view('yaaaro_pms/blogs', ['blog' => $blog]);
});

Route::get('/yaaaro_pms/blog_category', function () {
    $blogCategory = DB::table('blog_categories')->get();
    return view('yaaaro_pms/blog_category', [
        'blogCategory' => $blogCategory,
    ]);
});
Route::get('/yaaaro_pms/add_blog_category', function () {
    return view('yaaaro_pms/add_blog_category');
});

Route::get('/yaaaro_pms/add_blogs', function () {
    $writers = DB::table('writers')->get();
    $blogCategory = DB::table('blog_categories')->get();
    $subject = DB::table('subject_categories')->get();
    return view('yaaaro_pms/add_blogs', [
        'writers' => $writers,
        'blogCategory' => $blogCategory,
        'subject' => $subject,
    ]);
});
Route::get('/yaaaro_pms/writers_details', function () {
    return view('/yaaaro_pms/writers_details');
});

Route::get('/yaaaro_pms/writers_login', function () {
    return view('/yaaaro_pms/writers_login');
});

Route::get('/yaaaro_pms/edit_blogs', function () {
    return view('/yaaaro_pms/edit_blogs');
});

Route::get('/yaaaro_pms/subject_category', function () {
    $subject_category = DB::table('subject_categories')->get();
    return view('/yaaaro_pms/subject_category',[
        'subject_category'=>$subject_category,
    ]);
});

Route::get('/yaaaro_pms/add_subject_category', function () {
    return view('/yaaaro_pms/add_subject_category');
});

Route::get('/yaaaro_pms/status', function () {
    $status = DB::table('statuses')->get();
    return view('/yaaaro_pms/status',['status'=>$status]);
});
Route::get('/yaaaro_pms/add_status', function () {
    return view('/yaaaro_pms/add_status');
});